import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(
        private _cookieService: CookieService
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const idToken = this._cookieService.get("access_token");
        let cloned, token;

        if (idToken) {
            cloned = req.clone({
                headers: req.headers.set("access_token", idToken)
            });
        }
        else {
            cloned = req.clone();
        }

        return next.handle(cloned).pipe(
            tap(evt => {
                if (evt instanceof HttpResponse) {
                    token = evt.headers.get('access_token');
                    let expire = new Date();
                    let time = Date.now() + ((60 * 60 * 1000));
                    expire.setTime(time);
                    if (token) this._cookieService.set('access_token', token, expire);
                }
            })
        )
    }
}