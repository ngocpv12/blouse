export interface IContact {
    id: number,
    name: string,
    email: string,
    phoneNumber: string,
    title: string,
    message: string,
    respondentId: string,
    reply: string,
    isActive: number
}