import { MatSnackBar } from '@angular/material/snack-bar';
import { IMedicalExamination } from './../../Entity/medical-examination';
import { MatDialog } from '@angular/material/dialog';
import { MedicalExaminationService } from './../../Services/medical-examination.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DialogMedicalExaminationComponent } from '../general-components/dialog-medical-examination/dialog-medical-examination.component';

@Component({
  selector: 'app-medical-examination',
  templateUrl: './medical-examination.component.html',
  styleUrls: ['./medical-examination.component.css']
})
export class MedicalExaminationComponent implements OnInit {
  length: number;
  pageIndex: number = 1;
  pageSize: number = 5;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  dataSource;
  searchText;
  patientId;
  notFound;
  noExaminations: boolean;
  
  constructor(
    private _examinationService: MedicalExaminationService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  ngOnInit(): void {
    this.noExaminations = false;
    this._route.paramMap.subscribe((params: ParamMap) => {
      let patientId = params.get('p');
      this.patientId = patientId;
    });
    this.loadDataTable();
  }

  displayedColumns: string[] = [    
    'index',
    'id',
    'patient',
    'department',
    'doctor',
    'status',
    'appointmentTime',
    'place',
    'isActive',
    'action'
  ];

  loadDataTable = () => {
    console.log("I am loading dataTable");
    if (this.patientId) {
      this._examinationService.getExaminationByFilter(this.pageIndex, this.pageSize,this.patientId, null)
      .subscribe(response => {
        this.dataSource = response      
        let temp = this.dataSource.page;
        this.dataSource = this.dataSource.data;
        this.length = temp.total_item;
        this.dataSource = new MatTableDataSource(this.dataSource);
        this.dataSource.sort = this.sort;
      }, error => {
        console.log(error);
        this.noExaminations = true;
      });
    }
    else {
      this._examinationService.getAllExaminations(this.pageIndex, this.pageSize)
      .subscribe(response => {
        this.dataSource = response;
        console.log(this.dataSource);
        let temp = this.dataSource.page;
        this.length = temp.total_item;
        this.dataSource = this.dataSource.data;
        this.dataSource = new MatTableDataSource(this.dataSource);
        this.dataSource.sort = this.sort;
      });
    }
  }
  onPageChanged(event: PageEvent){
    console.log("Let see what event have");
    console.log(event);
    this.pageIndex = event.pageIndex+1;
    this.pageSize = event.pageSize;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
    this.searchPayment(this.pageIndex, this.pageSize , this.searchText);
    console.log("PageIndex:" + this.pageIndex+ ", PageSize: "+ this.pageSize);
  }
  searchPayment = (page,size,txtSearch) => {
    console.log("Executing search Patient");
    // CONTINUE HERE
    this._examinationService.getExaminationByFilter(page,size,null, txtSearch)
      .subscribe(response => {
        this.dataSource = response;
        console.log(this.dataSource);
        this.dataSource = this.dataSource.data;
        console.log(this.dataSource);
        this.dataSource = new MatTableDataSource(this.dataSource);
        this.dataSource.sort = this.sort;
        this.notFound = '';
      },
        error => {
          console.log("error -------------", error);
          this.notFound = "Rất tiếc chúng tôi không tìm thấy dữ liệu bạn cần";
          this.dataSource = "";
        }
      );
  }

  editMedicalExamination(examination: IMedicalExamination) {
    let dialogRef = this._dialog.open(DialogMedicalExaminationComponent, {
      data: {
        type: 'edit',
        pageIndex: this.pageIndex,
        pageSize: this.pageSize,
        id: examination.id,
        patient: examination.patient,
        patientId: examination.patient.id,
        doctor: examination.doctor,
        doctorId: examination.doctor.id,
        department: examination.doctor.department,
        departmentId: examination.doctor.department.id,
        status: examination.status,
        appointmentTime: examination.appointment.appointmentTime,
        place: examination.appointment.place,
        isActive: examination.isActive,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.openSnackBar(result.message, '');
        this.loadDataTable();
      }
    },
      error => console.log('Update Fail', error)
    );
  }

  // Function: set active on off
  setActive = (id) => {
    this._examinationService.setActive(id)
      .subscribe(response => {
        console.log("Set active successful!");
      });
  }

  reloadPage = () => {
    this._router.navigate(["/payments"]);
    this.patientId = null;
    this.noExaminations = false;
    this.loadDataTable();
  }
  
  openSnackBar(message: string, action: string) {
    let snackBarRef = this._snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      duration: 3000,
    });
  }
}
