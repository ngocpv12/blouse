import { MatSnackBar } from '@angular/material/snack-bar';
import { Validators } from '@angular/forms';
import { HospitalService } from 'src/app/Services/hospital.service';
import { DepartmentService } from 'src/app/Services/department.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { IDepartment } from 'src/app/Entity/department';
import { DialogDepartmentComponent } from '../general-components/dialog-department/dialog-department.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {

  colorSlide = "primary";
  //not found variable
  length: number;
  pageIndex: number = 1;
  pageSize: number = 5;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  notFound;
  searchText;
  dataSource;
  hospitals;
  departmentsByHosId;
  departmentById;
  hospitalId: number;
  hospitalById;
  selectedHospital;
  selectedDepartment;
  public errorMsg;
  constructor(
    private _dialog: MatDialog,
    private _departmentService: DepartmentService,
    private _hospitalService: HospitalService,
    private _router: Router,
    private _snackBar: MatSnackBar
  ) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  ngOnInit(): void {
    this.loadDataTable();
    this.loadHospitalInfo();
  }
  displayedColumns: string[] = ['index', 'id','name','address', 'phoneNumber', 'website', 'email', 'description', 'hospital', 'actions', 'isActive'];
  onPageChanged(event: PageEvent){
    console.log("Let see what event have");
    console.log(event);
    this.pageIndex = event.pageIndex+1;
    this.pageSize = event.pageSize;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
    this.searchDepartment(this.pageIndex, this.pageSize, this.selectedHospital,this.searchText);
    console.log("PageIndex:" + this.pageIndex+ ", PageSize: "+ this.pageSize);
  }
  loadDataTable() {
    console.log("I am loading dataTable");
    this._departmentService.getAllDepartments(this.pageIndex, this.pageSize)
      .subscribe(response => {
        this.dataSource = response;
        let temp = this.dataSource.page;
        this.length = temp.total_item;
        this.dataSource = this.dataSource.data;
        this.dataSource = new MatTableDataSource(this.dataSource);
        this.dataSource.sort = this.sort;

      },
      error => {console.log("error:",error)});
  }
  openDialog() {

    let dialogRef = this._dialog.open(DialogDepartmentComponent, {
      data: { type: 'create', dataSource: this.dataSource }
    });
    // dialogRef.updateSize("400px","500px");
    dialogRef.afterClosed().subscribe(result => { 
      console.log(`Dialog result: ${result}`);
        this.openSnackBar(result.message, '');
        this.loadDataTable();
    });
  }


  // function search doctor
  searchDepartment(page,size,hospitalId:number,txtSearch: string) {
    console.log("Executing search Patient");
    // CONTINUE HERE
    this._departmentService.getDepartmentByFilter(page,size,hospitalId,txtSearch)
      .subscribe(response => {
        this.dataSource = response;
        let temp = this.dataSource.page;
        this.dataSource = this.dataSource.data;
        this.length = temp.total_item;
        this.dataSource = new MatTableDataSource(this.dataSource);
        this.dataSource.sort = this.sort;
        this.notFound = '';
      },
        error => {
          console.log("error -------------", error);
          this.notFound = "Rất tiếc chúng tôi không tìm thấy dữ liệu bạn cần";
          this.dataSource = "";
        }
      );
  }
  editDepartment(department: IDepartment) {
    let dialogRef = this._dialog.open(DialogDepartmentComponent, {
      data: {
        type: 'edit',
        id: department.id,
        name: department.name,
        address: department.address,
        phoneNumber: department.phoneNumber,
        website: department.website,
        email: department.email,
        description: department.description,
        hospital: department.hospital,
        hospitalId: department.hospital.id,
        isActive: department.isActive
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      debugger
        this.loadDataTable();
        this.openSnackBar(result.message, '');
    },
      error => console.log('Update Fail', error)
    );
  }

  // Function: set active on off
  setActive(id) {
    this._departmentService.setActive(id)
      .subscribe(response => {
        console.log("Set active successful!");
      });
  }
    // load all hospital
    loadHospitalInfo() {
      console.log("I am loading hospitalInfo");
      this._hospitalService.getAllHospital()
        .subscribe(response => {
          this.hospitals = response;
          this.hospitals = this.hospitals.data;
          console.log(this.hospitals);
        });
    }

    resetPage(){
      console.log("Executing resetting page");
      
      this.pageIndex = 1;
      this.pageSize = 5;
      this.paginator.firstPage();
      this.pageSizeOptions = [5, 10, 25, 100];
    }
  
    openSnackBar(message: string, action: string) {
      let snackBarRef = this._snackBar.open(message, action, {
        verticalPosition: 'bottom',
        horizontalPosition: 'end',
        duration: 3000,
      });
    }
}
