import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PatientService } from 'src/app/Services/patient.service';
import { IPatient } from 'src/app/Entity/patient';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {
  colorSlide = "primary";
  //not found variable
  notFound;
  searchText;
  length: number;
  pageIndex: number = 1;
  pageSize: number = 5;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  dataSource;
  hospitals;
  departmentsByHosId;
  departmentById;
  hospitalId: number;
  hospitalById;
  selectedHospital;
  selectedDepartment;
  public errorMsg;
  constructor(
    private _dialog: MatDialog,
    private _patientService: PatientService,
    private _router: Router
  ) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  ngOnInit(): void {
    this.loadDataTable();
  }
  displayedColumns: string[] = ['index', 'id','username','name', 'gender', 'dob', 'title', 'email', 'phone', 'isActive', 'action'];

  loadDataTable() {
    console.log("I am loading dataTable");
    this._patientService.getAllPatients(this.pageIndex, this.pageSize)
      .subscribe(response => {
        this.dataSource = response;
        let temp = this.dataSource.page;
        this.dataSource = this.dataSource.data;
        this.length = temp.total_item;
        this.dataSource = new MatTableDataSource(this.dataSource);
        console.log(this.dataSource);
        this.dataSource.sort = this.sort;
      });
  }
  onPageChanged(event: PageEvent){
    console.log("Let see what event have");
    console.log(event);
    this.pageIndex = event.pageIndex+1;
    this.pageSize = event.pageSize;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
    this.searchPatient(this.pageIndex, this.pageSize,  this.searchText);
    console.log("PageIndex:" + this.pageIndex+ ", PageSize: "+ this.pageSize);
  }
  // function search doctor
  searchPatient(page,size,txtSearch: string) {
    console.log("Executing search Patient");
    // CONTINUE HERE
    this._patientService.getPatientByFilter(page,size,txtSearch)
      .subscribe(response => {
        this.dataSource = response;
        let temp = this.dataSource.page;
        this.dataSource = this.dataSource.data;
        this.length = temp.total_item;
        this.dataSource = new MatTableDataSource(this.dataSource);
        this.dataSource.sort = this.sort;
        this.notFound = '';
      },
        error => {
          console.log("error -------------", error);
          this.notFound = "Rất tiếc chúng tôi không tìm thấy dữ liệu bạn cần";
          this.dataSource = "";
        }
      );
  }
  // Function: set active on off
  setActive(id) {
    this._patientService.setActive(id)
      .subscribe(response => {
        console.log("Set active successful!");
      });
  } 

  viewExaminations = (patientId) => {
    this._router.navigate(["/medical-examination", { p: patientId, currentIndex: 'medical-examination' }]);
  }

  viewPayments = (patientId) => {
    this._router.navigate(["/payments", { p: patientId, currentIndex: 'payments' }]);
  }
  resetPage(){
    console.log("Executing resetting page");
    
    this.pageIndex = 1;
    this.pageSize = 5;
    this.paginator.firstPage();
    this.pageSizeOptions = [5, 10, 25, 100];
  }
}
