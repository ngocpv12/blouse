import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { SocketioService } from 'src/app/Services/socketio.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Client-Admin';
  loginStatus: boolean = false;
  notifications: string[] =[];
  private subscription;
  constructor(
    private _cookieService: CookieService,
    private _socketioService: SocketioService,
    private _router: Router,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void { 
    this.loginStatus = this._cookieService.check('access_token') ? true : false;
    if (this.loginStatus) {
      let token = this._cookieService.get('access_token');
      this.subscription = this._socketioService.getNotification(token).subscribe(notification =>{
        this.notifications.push(notification);
        this.openSnackBar(this.notifications[this.notifications.length -1], 'Xem');
      });
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  openSnackBar(message: string, action: string) {
    let snackBarRef = this._snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      duration: 5000,
    });
    snackBarRef.onAction().subscribe(() => {
      this._router.navigate(["/medical-examination"]);
    });
  }
}
