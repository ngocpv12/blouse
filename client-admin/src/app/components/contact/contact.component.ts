import { IContact } from './../../Entity/contact';
import { DialogContactComponent } from './../general-components/dialog-contact/dialog-contact.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ContactService } from 'src/app/Services/contact.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  length: number;
  pageIndex: number = 1;
  pageSize: number = 100;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  // MatPaginator Output
  pageEvent: PageEvent;
  dataSource;
  notFound;
  colorSlide = "primary";
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _contactService: ContactService,
    private _dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.loadDataTable();
  }
  displayedColumns: string[] = [
    'index', 
    'id',
    'name', 
    'email', 
    'phoneNumber', 
    'title', 
    'message', 
    'respondentId', 
    'Action'
  ];

  loadDataTable() {
    console.log("I am loading dataTable");
    this._contactService.getAllContacts(this.pageIndex, this.pageSize)
      .subscribe(response => {
        this.dataSource = response;
        console.log(this.dataSource);
        
        let temp = this.dataSource.page;
        this.dataSource = this.dataSource.data;
        this.length = temp.total_item;
        this.dataSource = new MatTableDataSource(this.dataSource);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
        error => { console.log("Khong co data bac si: ", error) });
  }

  // Function: set active on off
  setActive(id) {
    this._contactService.setActive(id)
      .subscribe(response => {
        console.log("Set active successful!");
      });
  }
  
  openDialog(type: string, id: number) {
    let data: IContact = this.dataSource.filteredData.find(e => e.id === id);
    data.title = '[REPLY] ' + data.title;
    let dialogRef = this._dialog.open(DialogContactComponent, {
      data: { 
        type: type,
        id: id,
        data: data,
       }
    });
    // dialogRef.updateSize("400px","500px");
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.openSnackBar(result.message, '');
      this.loadDataTable();
    });
  }
  
  openSnackBar(message: string, action: string) {
    let snackBarRef = this._snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      duration: 3000,
    });
  }
}
