import { HospitalService } from './../../../Services/hospital.service';
import { StaffService } from './../../../Services/staff.service';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';
import { forbiddenNameValidator } from 'src/app/validator/name.validator';

@Component({
  selector: 'app-dialog-staff',
  templateUrl: './dialog-staff.component.html',
  styleUrls: ['./dialog-staff.component.css']
})
export class DialogStaffComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private _staffService: StaffService,
    private _hospitalService: HospitalService,
    public dialogRef: MatDialogRef<DialogStaffComponent>
  ) { }

  ngOnInit(): void {
    debugger
    console.log(this.data);
    
    this.loadHospitals();
  }

  hospitals;
  addForm = this.fb.group({
    hospitalId: ['', Validators.required],
    isActive: ['', Validators.required],
    fullName: ['', [Validators.required, forbiddenNameValidator(/admin/)]],
    title: ['',Validators.required],
    gender: ['', Validators.required],
    phoneNumber: ['', [Validators.required,Validators.pattern('([0-9]{10})')]],
    dateOfBirth: ['', Validators.required],
    address: ['', Validators.required],
    email: ['', [Validators.required, Validators.pattern('^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$')]],
    username: ['', [Validators.required, forbiddenNameValidator(/admin/)]],
  });

  get hospitalId() {
    return this.addForm.get('hospitalId');
  }
  
  get isActive() {
    return this.addForm.get('isActive');
  }
  
  get fullName() {
    return this.addForm.get('fullName');
  }
  
  get title() {
    return this.addForm.get('title');
  }
  
  get gender() {
    return this.addForm.get('gender');
  }
  
  get phoneNumber() {
    return this.addForm.get('phoneNumber');
  }
  
  get dateOfBirth() {
    return this.addForm.get('dateOfBirth');
  }
  
  get address() {
    return this.addForm.get('address');
  }
  
  get email() {
    return this.addForm.get('email');
  }
  
  get username() {
    return this.addForm.get('username');
  }

  onSubmit(id) {
    if (id === undefined) {
      this._staffService.addStaff(this.addForm.value)
        .subscribe(
          response => {
            console.log('Success!', response);
            this.dialogRef.close(response);
          },
          error => {
            console.log('Error!', error);
            this.dialogRef.close(error);
          }
        );
    }
    else {
      console.log('update',id);
      this._staffService.updateStaff(id, this.addForm.value)
      .subscribe(
        response => {
          console.log('Success!', response);
          this.dialogRef.close(response);
        },
        error => {
          console.log('Error!', error);
          this.dialogRef.close(error);
        }
      );
    }
  }

  loadHospitals() {
    this._hospitalService.getAllHospital()
      .subscribe(
        response => {
          this.hospitals = response;
          this.hospitals = this.hospitals.data;
        },
        error => console.error('Error!', error)
      )
  }
}
