import { DoctorService } from './../../../Services/doctor.service';
import { environment } from './../../../../environments/environment';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HospitalService } from 'src/app/Services/hospital.service';
import * as fileSaver from 'file-saver';

@Component({
  selector: 'app-dialog-upload',
  templateUrl: './dialog-upload.component.html',
  styleUrls: ['./dialog-upload.component.css']
})
export class DialogUploadComponent implements OnInit {
  public addForm = this.fb.group({
    userRole: ['STAFF'],
    hospitalId: [''],
    file: ['']
  })
  public hospitals;
  public file2upload: File;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _hospitalService: HospitalService,
    private dialogRef: MatDialogRef<DialogUploadComponent>,
    private _doctorService: DoctorService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    if (this.data.userRole === 'ADMIN') {
      this.loadHospitalInfo();
    }
  }

  onSubmit() {
    let formData = new FormData();
    formData.append('role', this.addForm.value.userRole);
    formData.append('hosId', this.addForm.value.hospitalId);
    formData.append('excel', this.addForm.value.file);
    this._doctorService.uploadExcelFile(formData)
    .subscribe(response => {
      console.log(response);
      this.dialogRef.close(response);
    }, error => {
      console.log('get error ', error);
      this.dialogRef.close(error);
    });
  }

  loadHospitalInfo() {
    console.log("I am loading hospitalInfo");
    this._hospitalService.getAllHospital()
      .subscribe(response => {
        this.hospitals = response;
        this.hospitals = this.hospitals.data;
        console.log(this.hospitals);
      });
  }

  getTemplateFile() {
    this._doctorService.getTemplateFile()
    .subscribe(response => {
      let blob:any = new Blob([response], { type: 'text/json; charset=utf-8' });
			fileSaver.saveAs(blob, 'blouse-doctor.xlsx');
    }, error => {
      console.log('get error ', error);
    });
  }
}
