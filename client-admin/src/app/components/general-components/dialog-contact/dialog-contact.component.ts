import { IContact } from './../../../Entity/contact';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ContactService } from 'src/app/Services/contact.service';
import { CookieService } from 'ngx-cookie-service';
import decode from 'jwt-decode';

@Component({
  selector: 'app-dialog-contact',
  templateUrl: './dialog-contact.component.html',
  styleUrls: ['./dialog-contact.component.css']
})
export class DialogContactComponent implements OnInit {
  public replyMessage: FormControl = new FormControl(this.data.data.reply);

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DialogContactComponent>,
    private _contactService: ContactService,
    private _cookieService: CookieService
  ) { }

  ngOnInit(): void {
    console.log(this.data);
    console.log(this.replyMessage);
  }

  send() {
    let token = this._cookieService.get('access_token');
    let tokenPayload = decode(token);
    let data: IContact = {
      id: this.data.data.id,
      name: this.data.data.name,
      email: this.data.data.email,
      phoneNumber: this.data.data.phoneNumber,
      title: this.data.data.title,
      message: this.data.data.message,
      respondentId: tokenPayload.id,
      reply: this.replyMessage.value,
      isActive: this.data.data.isActive
    }
    this._contactService.reply(data)
    .subscribe(response => {
      this.dialogRef.close(response);
    }, error => {
      this.dialogRef.close(error);
    })
  }

  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '0',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['bold', 'italic'],
      ['fontSize']
    ]
  };
}
