import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';
import decode from 'jwt-decode';
  import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketioService {
  constructor() { }

  private url = environment.SOCKET_ENDPOINT;  
  private socket;
  
  getNotification(token): Observable<string> {
    let observable = new Observable<string>(observer => {
      let tokenPayload = decode(token);
      if(tokenPayload.role === "STAFF") {
        this.socket = io(this.url);
        this.socket.emit("staffLogin", {
          "staffId": tokenPayload.id
        });
        this.socket.on('newRequest', (message) => {
          let notification: string = message;
          observer.next(notification);
        });
      }
      return () => {
        this.socket.disconnect();
      };
    })
    return observable;
  }

  confirmRequest(token, userId, doctorId): Observable<string> {
    let observable = new Observable<string>(observer => {
      let tokenPayload = decode(token);
      if(tokenPayload.role === "STAFF") {
        this.socket = io(this.url);
        this.socket.emit("confirmed", {
          staffId: tokenPayload.id,
          userId: userId,
          doctorId: doctorId
        });
        observer.next();
      }
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
}
