import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IContact } from '../Entity/contact';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private _http: HttpClient) { }
  _url = environment.SERVER_URL + 'contact';

  getAllContacts(page: number = null,size: number = null): Observable<IContact[]> {
    let getUrl = this._url+"?";
    if(page != null){
      getUrl = getUrl + "page=" + page + "&";
    }
    if(size != null){
      getUrl = getUrl + "size=" + size +"&";
    }
    return this._http.get<IContact[]>(getUrl);
  }

  reply(con: IContact) {
    return this._http.put(this._url + '/reply', con);
  }

  setActive(id) {
    return this._http.put<any>(this._url + "/setActive/" + id ,id);
  }
}
