class Ultis {
    static minifyString = (s: string) => {
        return s.normalize('NFD')
            .replace(/[\u0300-\u036f]/g, '')
            .replace(/[đĐ]/g, m => m === 'đ' ? 'd' : 'D')
            .replace(/ + /g," ")
            .trim()
            .toLowerCase();
    }

    static removeDuplicates(myArr, prop) {
        return myArr.filter((obj, pos, arr) => {
            return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos
        });
    }
}


export default Ultis;