export interface IContact {
    id: number,
    name: string,
    email: string,
    phoneNumber: string,
    title: string,
    message: string,
    respondentId: string,
    isActive: number,
    createdAt: Date,
    modifiedAt: Date
}