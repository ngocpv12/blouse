export interface IMedicineMedicalRecord {
    id: number;
    medicalRecordId: number;
    medicineId: number;
    quantity: number;
    unit: string; 
    note: string;
}