import { IMedicineMedicalRecord } from './medicine-medical-record';
import { IMedicalExamination } from './medical-examination';

export interface IMedicalRecord{
    id: number,
    description: string,
    pathology: string,
    treatment: string,
    medicalExamination: IMedicalExamination;
    medicineMedicalRecords: IMedicineMedicalRecord[];
}