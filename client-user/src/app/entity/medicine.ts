
export interface IMedicine {
    id: number;
    name: string;
    active_material: string;
    classification: string;
    concentration: string;
    excipient: string;
    preparation: string;
    packaging: string;
    lifespan: string;
    made_by: string;
    is_active: string;
}