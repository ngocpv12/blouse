import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { FeedbackService } from 'src/app/Services/feedback.service';
import { ExaminationService } from 'src/app/Services/examination.service';
import { CookieService } from 'ngx-cookie-service';
import decode from 'jwt-decode';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SocketioService } from 'src/app/Services/socketio.service';
import {Location} from '@angular/common';
@Component({
  selector: 'app-doctor-detail',
  templateUrl: './doctor-detail.component.html',
  styleUrls: ['./doctor-detail.component.css']
})
export class DoctorDetailComponent implements OnInit {

  doctorId;
  name;
  departmentName;
  hospitalName;
  feedbackAvgScore;
  image;
  selectedDoctor;
  selectedIdDoctor;
  datePicker;
  dateRequired;
  loggedIn;
  description;
  voteCount;
  todayDate;
  role: string;
  // message to notice request success
  noticeRequestSuccess: boolean = false;
  noticeRequestFail: boolean = false;
  noticeRequestExisted: boolean = false;
  isValidDate: boolean;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private feedback: FeedbackService,
    private _examination: ExaminationService,
    private cookieService: CookieService,
    private _snackBar: MatSnackBar,
    private _socketioService: SocketioService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let doctorId = parseInt(params.get('doctorId'));
      this.doctorId = doctorId;
      let name = params.get('name');
      this.name = name;
      let departmentName = params.get('departmentName');
      this.departmentName = departmentName;
      let hospitalName = params.get('hospitalName');
      this.hospitalName = hospitalName;
      let image = params.get('image');
      this.image = image;
      this.loggedIn = this.cookieService.check('access_token');
    });
    if(this.checkLoggedIn()){

      const token = this.cookieService.get('access_token');
      const tokenPayload = decode(token);
      this.role = tokenPayload.role;
    }

    this.getFeedbackAvgScore();
    this.getTodayDate();
  }

  getFeedbackAvgScore() {

    this.feedback.getFeedbackbyDoctorId(this.doctorId)
      .subscribe((response) => {

        let tempdata = response;
        this.feedbackAvgScore = tempdata.averageStar;
        this.voteCount = tempdata.totalRating;
        console.log(tempdata);
      },
        (err) => {
          if (err.error.message == "Feedback Not Found.") {
            this.voteCount = 0;
          } else {
            console.log(err);
          }
        });

  }
  book() {
    this.router.navigate(['/schedule', { doctorId: this.doctorId, doctorName: this.name }]);
  }
  back() {
    this.location.back();
  }
  toggle() {
    var blur = document.getElementById('blur');
    blur.classList.toggle('active');

    var popup = document.getElementById('popup');
    popup.classList.toggle('active');
  }
  toggle2() {

    var popup2 = document.getElementById('popup2');
    popup2.classList.toggle('active');
  }
  sendRequest(doctorId, description, date) {
    this._examination.requestExamination(doctorId, description, date)
      .subscribe(response => {
        console.log("Gui yeu cau thanh cong", response);
        this.openSnackBar("Gửi yêu cầu khám thành công. Quý khách hãy đợi phản hồi của chúng tôi.","Quản lí lịch khám");
        this.noticeRequestSuccess = true;
        this.noticeRequestFail = false;
        this.noticeRequestExisted= false;
        
        let token = this.cookieService.get('access_token');
        this._socketioService.sendRequest(token, doctorId).subscribe();
      },
      err => {
        let temp = err;
        let tempErr = temp.error.message;
        if (tempErr == "Register Medical Examination Existed.") {
          this.openSnackBar("Gửi yêu cầu thất bại. Bạn hãy đặt lịch với bác sĩ khác hoặc đợi yêu cầu gần nhất của bạn được xác nhận.","");
        } else {
          this.openSnackBar("Đã có lỗi hệ thống xảy ra. Chúng tôi rất tiếc vì điều này. \
          Hãy liên hệ cho ban quản trị để thông báo về lỗi. Xin chân thành cảm ơn!","");
          this.noticeRequestSuccess = false;
        }
      });
    this.toggle();
  }
  displayError() {
    if (!this.datePicker) {
      this.dateRequired = "Bạn phải chọn một ngày để khám!";
    }
    console.log(this.datePicker);
  }

  checkLoggedIn() {
    return this.cookieService.check('access_token');
  }

  openSnackBar(message: string, action: string) {
    let snackBarRef = this._snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      duration: 10000,
    });
    snackBarRef.onAction().subscribe(()=>{
      this.router.navigate(["/manage-index/manage-request"]);
    });
  }


  getTodayDate() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    this.todayDate = dd + "/" + mm + "/" + yyyy;
  }

  checkValidTime() {
    var today = new Date();
    var newDate = new Date(this.datePicker);
    if (newDate.getTime() <= today.getTime()) {
      this.isValidDate = false;
    }else{
      this.isValidDate = true;
    }
  }
  changeTime(){
    console.log("change time running");
    this.checkValidTime();
  }
}
