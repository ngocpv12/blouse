import { environment } from './../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IDoctor } from 'src/app/entity/doctor';
import { DoctorService } from 'src/app/Services/doctor.service';
import { IUser } from 'src/app/entity/user';
import { SessionService } from 'src/app/Services/session.service';
import { HospitalService } from 'src/app/Services/hospital.service';
import { DepartmentService } from 'src/app/Services/department.service';
import { LoginComponent } from '../login/login.component';


@Component({
  selector: 'app-doctor-page',
  templateUrl: './doctor-page.component.html',
  styleUrls: ['./doctor-page.component.css']
})
export class DoctorPageComponent implements OnInit {

  constructor(
    private _router: Router,
    private _doctorServices: DoctorService,
    private _hospitalService: HospitalService,
    private _departmentService: DepartmentService
  ) { }
  // paging
  pageIndex: number=1;
  pageChanged(event: Event){
 
    this.pageIndex =<number>(<unknown>event);
    this.searchDoctor(this.pageIndex,this.pageSize,this.selectedHospital, this.selectedDepartment, this.searchText);
  }
  ngOnInit(): void {
    this.loadAllDoctor(this.pageIndex,this.pageSize);
    this.loadHospitalInfo();

  }


  
  hospitals;
  // declare variable array doctor
  listDoctors: any = [];
  listUsers: Array<IUser>;
  // count doctor
  totalDoctors: number;
  pageSize: number = 8;
  searchText;
  selectedHospital = 0;
  selectedDepartment = 0;
  notFound;
  departmentsByHosId;
  
  server_url = environment.SERVER_URL;
  loadAllDoctor(pageIndex, pageSize) {
    console.log("Load doctor");
    this._doctorServices.getAllDoctors(pageIndex,pageSize)
      .subscribe(response => {
        this.listDoctors = response;
        let temp = this.listDoctors.page;
        console.log('Doctors: ', this.listDoctors);
        this.listDoctors = this.listDoctors.data;
        this.totalDoctors = temp.total_item;
      })
  }
  // onSelect(id){
  //   this.router.navigate(['/doctor', id]);
  // }

  // function on select each card
  onSelect(doctor: IDoctor){

    this._router.navigate(['/doctor', doctor.id,{
      doctorId: doctor.id,
      name: doctor.user.fullName,
      departmentName: doctor.department.name,
      hospitalName: doctor.department.hospital.name,
      image:  this.server_url + doctor.image
    }]);
    
  }

  // function search doctor
  searchDoctor(page,size,hospitalId: number = null, departmentId: number = null, txtSearch: string) {
    if(hospitalId == 0){
      hospitalId = null;
    }
    if(departmentId == 0){
      departmentId = null;
    }
    console.log("Executing search doctor");
    console.log("hospitalId:" + hospitalId + "departmentId:" + departmentId + "txtSearch:" + txtSearch);
 
    this._doctorServices.getDoctorByFilter(page,size,hospitalId, departmentId, txtSearch)
      .subscribe(response => {
        this.listDoctors = response;
        let temp = this.listDoctors.page;
        this.listDoctors = this.listDoctors.data;
        this.notFound = '';
        this.totalDoctors = temp.total_item;
      },
        error => {
          console.log("error -------------", error);
          this.notFound = "Rất tiếc! Chúng tôi không tìm thấy bác sĩ mà bạn muốn";
          this.listDoctors = [];
          console.log("list doctor");
          console.log( this.listDoctors);
        }
      );

  }

  // load all hospital
  loadHospitalInfo() {
    console.log("I am loading hospitalInfo");
    this._hospitalService.getAllHospital()
      .subscribe(response => {
        this.hospitals = response;
        console.log(this.hospitals);
      });
  }
  // load all department by id
  loadDepartmentByHosId(id) {
    console.log("Loading department with hospital id:" + id);
    this._departmentService.getDepartmentByHosId(id)
      .subscribe((response) => {
        this.departmentsByHosId = response;
        console.log("List department by id" + id + "----------------");
        console.log(this.departmentsByHosId);
      }, error => {
        this.departmentsByHosId = [];
      });
      this.selectedDepartment = 0;
  }
  resetPage(){
    this.pageIndex = 1;
    this.pageSize = 8;
  }
}
