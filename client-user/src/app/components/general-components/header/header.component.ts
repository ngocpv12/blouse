
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/Services/authentication.service';
import { CookieService } from 'ngx-cookie-service';
import { SocialAuthService } from 'angularx-social-login';
import { PatientService } from 'src/app/Services/patient.service';
import { environment } from 'src/environments/environment';
import decode from 'jwt-decode';
import { DoctorService } from 'src/app/Services/doctor.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  currentPatient;
  username;
  currentDoctor;
  constructor(
    private _auth: AuthenticationService,
    private _cookieService: CookieService,
    private authService: SocialAuthService,
    private _patientSerivce: PatientService,
    private _doctorService: DoctorService
  ) { }
  loginStatus: boolean = false;
  server_url = environment.SERVER_URL;
  role: string;
  ngOnInit(): void {
    console.log("login status");
    const token = this._cookieService.get('access_token');
    const tokenPayload = decode(token);
    this.role = tokenPayload.role;
    if (this.checkLoggedIn()) {
      this.loginStatus = true;
      if (tokenPayload.role == "PATIENT") {
        this._patientSerivce.getPatientById()
          .subscribe(response => {
            this.currentPatient = response;
            this.username = this.currentPatient.user.username;
            console.log("alolooo");
            console.log(this.currentPatient);
          });
      }else if(tokenPayload.role == "DOCTOR") {
        this._doctorService.getDoctorbyUserId()
          .subscribe(response => {
            this.currentDoctor = response;
            this.username = this.currentDoctor.user.username;
            console.log("CURRENT DOCTOR");
            console.log(this.currentDoctor);
            
            
          });
      }

    } else {
      this.loginStatus = false;
    }
    console.log(this.loginStatus);

  }
  checkLoggedIn() {
    return this._cookieService.check('access_token');
  }
  logout() {
    this._auth.logout();
    this.loginStatus = false;
    this.signOut();
    console.log("log out successfully!");
    window.location.href = "login";
  }
  changeStatus(value) {
    this.loginStatus = value;
  }

  signOut(): void {
    this.authService.signOut();
  }

}
