import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotiStatusComponent } from './noti-status.component';

describe('NotiStatusComponent', () => {
  let component: NotiStatusComponent;
  let fixture: ComponentFixture<NotiStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotiStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotiStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
