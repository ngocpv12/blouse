import { ExaminationService } from 'src/app/Services/examination.service';
import { MedicineService } from './../../../Services/medicine.service';
import { IMedicine } from './../../../entity/medicine';
import { MedicalRecordService } from './../../../Services/medical-record.service';
import { Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { IMedicalRecord } from './../../../entity/medical-record';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { take, takeUntil } from 'rxjs/operators';
import Ultis from 'src/app/helpers/ultis';

@Component({
  selector: 'app-close-examination-form',
  templateUrl: './close-examination-form.component.html',
  styleUrls: ['./close-examination-form.component.css']
})
export class CloseExaminationFormComponent implements OnInit {
  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;

  medicalRecord
  rawMedicines;
  medicines: IMedicine[] = [];
  medicineMedicalRecords = [];

  public descriptionCtrl: FormControl = new FormControl();
  public pathologyCtrl: FormControl = new FormControl();
  public treatmentCtrl: FormControl = new FormControl();
  public quantityCtrl: FormControl = new FormControl();
  public unitCtrl: FormControl = new FormControl();
  public noteCtrl: FormControl = new FormControl();

  public medicineCtrl: FormControl = new FormControl();
  public medicineFilterCtrl: FormControl = new FormControl();
  public filteredMedicines: ReplaySubject<IMedicine[]> = new ReplaySubject<IMedicine[]>(1);
  protected _onDestroy = new Subject<void>();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<CloseExaminationFormComponent>,
    private _medicalRecordService: MedicalRecordService,
    private _medicineService: MedicineService,
    private _examationService: ExaminationService
  ) { }

  ngOnInit(): void {    
    this.loadMedicalRecord(this.data.examId);
    this.filteredMedicines.next(this.medicines.slice());
    this.medicineFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterMedicines();
    });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  onSubmit() {
    if (this.descriptionCtrl.value && this.pathologyCtrl.value && this.treatmentCtrl.value && this.medicineMedicalRecords.length) {
      let record = {
        id: this.medicalRecord.id,
        description: this.descriptionCtrl.value,
        pathology: this.pathologyCtrl.value,
        treatment: this.treatmentCtrl.value,
        examId: this.data.examId,
        medicineMedicalRecords: this.medicineMedicalRecords
      }
      this._examationService.closeExamination(this.data.examId, record)
      .subscribe(response => {
        console.log(response);
        this.dialogRef.close(response);
      }, error => {
        console.log(error);
        this.dialogRef.close(error);
      });
    }
  }

  loadMedicalRecord(examId) {
    this._medicalRecordService.getRecordByExamId(examId)
    .subscribe(response => {
      this.medicalRecord = response;
      console.log(this.medicalRecord);
    });
  }

  addMedicine() {
    debugger
    if (this.medicineCtrl.value && this.quantityCtrl.value && this.unitCtrl.value && this.noteCtrl.value) {
      let name = this.medicines.find(med => med.id === this.medicineCtrl.value)['name'];
      this.medicineMedicalRecords.push({
        medicalRecordId: this.medicalRecord.id,
        medicineId: this.medicineCtrl.value,
        medicineName: name,
        quantity: this.quantityCtrl.value,
        unit: this.unitCtrl.value,
        note: this.noteCtrl.value
      });
      this.medicineCtrl.reset();
      this.quantityCtrl.reset();
      this.unitCtrl.reset();
      this.noteCtrl.reset();
    }
  }

  //ngx-mat-select-search, do not touch
  ngAfterViewInit() {
    this.setInitialValue();
  }

  protected setInitialValue() {
    this.filteredMedicines
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelect.compareWith = (a: IMedicine, b: IMedicine) => a && b && a.id === b.id;
      });
  }

  protected filterMedicines() {
    if (!this.medicines) {
      return;
    }
    let search = this.medicineFilterCtrl.value;
    if (!search) {
      this.filteredMedicines.next(this.medicines.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    if(search.length >= 3) {
      this._medicineService.searchMedicine(search)
      .subscribe(response => {
        this.medicines.length = 0;
        this.rawMedicines = response;
        this.rawMedicines = this.rawMedicines.data;
        this.medicines = Ultis.removeDuplicates(this.rawMedicines, 'name');
        console.log(this.medicines);
        this.filteredMedicines.next(
          this.medicines.filter(medicine => medicine.name.toLowerCase().indexOf(search) > -1)
        );
      }, error => {
        console.log(error);
      });
    } 
    // else if (search.length < 3) {     
    //   debugger 
    //   this.medicines.length = 0;
    //   this.filteredMedicines.next(
    //     this.medicines.filter(medicine => medicine.name.toLowerCase().indexOf(search) > -1)
    //   );
    // }
    else {
      this.filteredMedicines.next(
        this.medicines.filter(medicine => medicine.name.toLowerCase().indexOf(search) > -1)
      );
    }
  }
}
