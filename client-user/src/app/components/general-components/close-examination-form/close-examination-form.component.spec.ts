import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloseExaminationFormComponent } from './close-examination-form.component';

describe('CloseExaminationFormComponent', () => {
  let component: CloseExaminationFormComponent;
  let fixture: ComponentFixture<CloseExaminationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloseExaminationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseExaminationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
