import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/Services/authentication.service';
import { PatientService } from 'src/app/Services/patient.service';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService, SocialUser } from "angularx-social-login";
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user: SocialUser;
  loggedIn: boolean;
  alreadyRegisted;
  constructor(
    private fb: FormBuilder, 
    private _router: Router,
    private _patient: PatientService,
    private authService: SocialAuthService,
    private _authenService: AuthenticationService
    ) { }

  ngOnInit(): void {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      this._authenService.loginWithGoogle(user.name, user.email).subscribe((response)=> {
        window.location.href = "home";
      });
    });
  }
  get username() {
    return this.addForm.get('username');
  }
  get password() {
    return this.addForm.get('password');
  }
  get rePassword() {
    return this.addForm.get('rePassword');
  }
  get email() {
    return this.addForm.get('email');
  }
  addForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
    rePassword: ['', Validators.required],
    email: ['', [Validators.required, Validators.pattern('^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$')]]
  });

  onSubmit(){
    this._patient.register(this.addForm.value)
      .subscribe(response => {
        console.log("Register successfully");
        this._router.navigate(["/register-pending", {message: "Chúng tôi đã gửi mail xác nhận về email của quý khách."}]);
        this.alreadyRegisted = "";
      },
      error => {
        console.log("register fail", error);
        if(error.error.message = "User already registered."){
          this.alreadyRegisted = "Tên đăng nhập hoặc địa chỉ email đã bị sử dụng"
        }
      });
  }
  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
}
