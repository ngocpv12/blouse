import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageConfirmedByDoctorComponent } from './manage-confirmed-by-doctor.component';

describe('ManageConfirmedByDoctorComponent', () => {
  let component: ManageConfirmedByDoctorComponent;
  let fixture: ComponentFixture<ManageConfirmedByDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageConfirmedByDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageConfirmedByDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
