import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IMedicalRecord } from 'src/app/entity/medical-record';
import { ExaminationService } from 'src/app/Services/examination.service';
import { MedicalRecordService } from 'src/app/Services/medical-record.service';
import { environment } from 'src/environments/environment';
import { CloseExaminationFormComponent } from '../general-components/close-examination-form/close-examination-form.component';

@Component({
  selector: 'app-manage-confirmed-by-doctor',
  templateUrl: './manage-confirmed-by-doctor.component.html',
  styleUrls: ['./manage-confirmed-by-doctor.component.css']
})
export class ManageConfirmedByDoctorComponent implements OnInit {
  server_url = environment.SERVER_URL;
  listMedicalExamByDoctor;
  medicalRecord: IMedicalRecord;
  today = new Date();

  constructor(
    private _examination: ExaminationService,
    private _medicalRecordService: MedicalRecordService,
    private _dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.getMedicalExaminationByDoctor();
  }

  getMedicalExaminationByDoctor() {
    this._examination.getExaminationByDoctor()
      .subscribe(response => {
        console.log("data medi exam:");
        this.listMedicalExamByDoctor = response;
        this.listMedicalExamByDoctor =  this.listMedicalExamByDoctor.data;
        this.listMedicalExamByDoctor = this.listMedicalExamByDoctor
          .filter(examination => examination.status === 1 || examination.status === 2);
        console.log(this.listMedicalExamByDoctor);
      });
  }

  togglePopup = (type: string, exam) => {
    document.getElementById(type + "-" + exam.id).classList.toggle("active");
  }

  openCloseDialog(examId) {
    let dialogRef = this._dialog.open(CloseExaminationFormComponent, {
      data: {
        examId: examId
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.openSnackBar(result.message, null);
      this.getMedicalExaminationByDoctor();
    })
  }

  openSnackBar(message: string, action: string) {
    let snackBarRef = this._snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      duration: 3000,
    });
  }
  
  compareDate(rawDate1, rawDate2) {
    let d1 = new Date(rawDate1);
    let d2 = new Date(rawDate2);

    return d1.setHours(0, 0, 0, 0) >= d2.setHours(0, 0, 0, 0);
  }
}
