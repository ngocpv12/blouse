import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { DoctorService } from 'src/app/Services/doctor.service';
import { environment } from 'src/environments/environment';
import decode from 'jwt-decode';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
@Component({
  selector: 'app-personal-image',
  templateUrl: './personal-image.component.html',
  styleUrls: ['./personal-image.component.css']
})
export class PersonalImageComponent implements OnInit {

  constructor(
    private _doctorService: DoctorService,
    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private router: Router

  ) { }
  currentDoctor;
  imagePath;
  imageURL;
  imgURL;
  message;
  selectedFile;
  server_url = environment.SERVER_URL;
  get file() {
    return this.addForm.get('file');
  }
  addForm = this.fb.group({
    file: ['', Validators.required]
  });
  preview(files) {    
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
    this.selectedFile = files[0];    
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];    
  }
  ngOnInit(): void {
    this._doctorService.getDoctorbyUserId()
    .subscribe(response => {
      this.currentDoctor = response;
      this.imageURL = this.server_url+ this.currentDoctor.image;
      console.log("Image URL");
      
      console.log(this.currentDoctor);
      
    });
  }
  onSubmit(){
    console.log("uploading image");
    console.log("Selected file in submit");
    console.log(this.selectedFile);
    
    this._doctorService.uploadImage(this.selectedFile)
      .subscribe(response => {
        console.log(response);
      });
  }

  openSnackBar(message: string, action: string) {
    let snackBarRef = this._snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      duration: 10000,
    });
  }
}
