import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthenticationService } from 'src/app/Services/authentication.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  constructor(
    private authService: AuthenticationService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
  }
  confirm(email){
    this.authService.resetPassword(email)
      .subscribe(response => {
        console.log(response);
        this.openSnackBar(response.message, '');
      });
  }
  
  openSnackBar(message: string, action: string) {
    let snackBarRef = this._snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      duration: 5000,
    });
  }
}
