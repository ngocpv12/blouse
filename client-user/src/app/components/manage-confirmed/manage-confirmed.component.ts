import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ExaminationService } from 'src/app/Services/examination.service';
import { MedicalRecordService } from 'src/app/Services/medical-record.service';
import { PaymentService } from 'src/app/Services/payment.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-manage-confirmed',
  templateUrl: './manage-confirmed.component.html',
  styleUrls: ['./manage-confirmed.component.css']
})
export class ManageConfirmedComponent implements OnInit {

  constructor(
    private _examinationService: ExaminationService,
    private _paymentService: PaymentService,
    private _medicalRecordService: MedicalRecordService,
    private _snackBar: MatSnackBar
  ) { }
  listMedicalExamination;
  momoResponse;
  server_url = environment.SERVER_URL;
  medicalRecord;
  ngOnInit(): void {
    this.getMedicalExamination();
  }

  getMedicalExamination(){
    this._examinationService.getExaminationByFilter()
      .subscribe(response => {
        console.log("data medi exam:");
        this.listMedicalExamination = response;
        this.listMedicalExamination =  this.listMedicalExamination.data;
        this.listMedicalExamination = this.listMedicalExamination
          .filter(examination => examination.status === 1);
        console.log(this.listMedicalExamination);
      });
  }

  getPayment = (data) => {
    this._paymentService.getPayUrl(data)
      .subscribe(
        response => {
          this.momoResponse = response;
          document.location.href = this.momoResponse.payUrl;
        }, error => {
          this.openSnackBar("Có lỗi xảy ra, vui lòng thử lại", "");
        }
      );
  }

  cancelExamination(examId) {
    this._examinationService.cancelExamination({ id: examId })
    .subscribe(
      response => {
        this.getMedicalExamination();
        this.openSnackBar(response.message, "");
      }, error => {
        this.openSnackBar(error.message, "");
      }
    );
  }

  loadMedicalRecord(exam) {
    if (exam.status === 3) {
      this._medicalRecordService.getRecordByExamId(exam.id)
      .subscribe(response => {
        this.medicalRecord = response;
        console.log(this.medicalRecord);
        
        document.getElementById("detail-" + exam.id).classList.toggle("active");
      })
    }
    else {
      this.togglePopup(exam, 'detail');
    }
  }

  togglePopup = (exam, type) => {
    document.getElementById(type + "-" + exam.id).classList.toggle("active");
  }

  openSnackBar(message: string, action: string) {
    let snackBarRef = this._snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      duration: 3000,
    });
  }
}