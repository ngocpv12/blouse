import { CookieService } from 'ngx-cookie-service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AuthenticationService } from 'src/app/Services/authentication.service';
import { DoctorService } from 'src/app/Services/doctor.service';
import { HospitalService } from 'src/app/Services/hospital.service';
import { DepartmentService } from 'src/app/Services/department.service';
import { ExaminationService } from 'src/app/Services/examination.service';
import { environment } from 'src/environments/environment';
import { SocketioService } from './../../Services/socketio.service';
import { MatSnackBar } from '@angular/material/snack-bar';

declare const nextPrev: any;


@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {
  // selected id doctor
  selectedIdDoctor;
  // check if doctor is not chose
  todayDate;
  doctorRequired;
  dateRequired;
  errorMessage: string;
  hospitals;
  selectedDoctor;
  // declare variable array doctor
  listDoctors: any = [];
  datePicker;
  // count doctor
  totalDoctors: number;
  searchText;
  selectedHospital = 0;
  selectedDepartment = 0;
  selectedHospitalOld;
  notFound;
  departmentsByHosId;
  toggleForm: boolean = false;
  description = "";
  // message to notice request success
  noticeRequestSuccess: boolean = false;
  noticeRequestFail: boolean = false;
  noticeRequestExisted: boolean = false;
  pageIndex: number = 1;
  pageSize: number = 4;
  isValidDate: boolean;
  server_url = environment.SERVER_URL;
  constructor(
    private fb: FormBuilder,
    private _router: Router,
    private _doctorServices: DoctorService,
    private _hospitalService: HospitalService,
    private _departmentService: DepartmentService,
    private _examination: ExaminationService,
    private route: ActivatedRoute,
    private _cookieService: CookieService,
    private _socketioService: SocketioService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.loadHospitalInfo();
    this.getTodayDate();
    this.route.paramMap.subscribe((params: ParamMap) => {
      let doctorId = parseInt(params.get('doctorId'));
      this.selectedIdDoctor = doctorId;
      let name = params.get('doctorName');
      this.selectedDoctor = name;
    });
    console.log("selected name doctor " + this.selectedDoctor);
    console.log("selected id doctor " + this.selectedIdDoctor);
  }

  getTodayDate() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    this.todayDate = dd + "/" + mm + "/" + yyyy;
  }
  onSubmit() {
    console.log("I am doing submit!");

  }
  pageChanged(event: Event) {

    this.pageIndex = <number>(<unknown>event);
    this.searchDoctor(this.pageIndex, this.pageSize, this.selectedHospital, this.selectedDepartment, this.searchText);
  }

  // function search doctor
  searchDoctor(page, size, hospitalId: number = null, departmentId: number = null, txtSearch: string) {
    if (hospitalId == 0) {
      hospitalId = null;
    }

    if (departmentId == 0) {
      departmentId = null;
    }
    console.log("hospitalId:" + hospitalId + "departmentId:" + departmentId + "txtSearch:" + txtSearch);
    this._doctorServices.getDoctorByFilter(page, size, hospitalId, departmentId, txtSearch)
      .subscribe(response => {
        this.listDoctors = response;
        let temp = this.listDoctors.page;
        this.listDoctors = this.listDoctors.data;
        this.notFound = '';
        this.totalDoctors = temp.total_item;
      },
        error => {
          console.log("error -------------");
          this.notFound = "Rất tiếc! Chúng tôi không tìm thấy bác sĩ mà bạn muốn";
          this.listDoctors = [];
        }

      );
    this.selectedHospitalOld = hospitalId;
    this.doctorRequired = "";
  }
  // load all hospital
  loadHospitalInfo() {
    console.log("I am loading hospitalInfo");
    this._hospitalService.getAllHospital()
      .subscribe(response => {
        this.hospitals = response;
        console.log(this.hospitals);
      });
  }
  // load all department by id
  loadDepartmentByHosId(id) {
    console.log("Loading department with hospital id:" + id);
    this._departmentService.getDepartmentByHosId(id)
      .subscribe((response) => {
        this.departmentsByHosId = response;
        console.log("List department by id" + id + "----------------");
        console.log(this.departmentsByHosId);
      }, error => {
        this.departmentsByHosId = [];
      });
    this.selectedDepartment = 0;
  }


  displayError() {
    console.log("I am displaying error!");
    this.notFound = "";
    if (!this.selectedIdDoctor && !this.datePicker) {
      this.doctorRequired = "Bạn phải đặt lịch với một bác sĩ!";
      this.dateRequired = "Bạn phải chọn một ngày để khám!";
    } else if (!this.selectedIdDoctor) {
      this.doctorRequired = "Bạn phải đặt lịch với một bác sĩ!";
    } else if (!this.datePicker) {
      this.dateRequired = "Bạn phải chọn một ngày để khám!";
    }
  }
  toggle() {
    var blur = document.getElementById('blur');
    blur.classList.toggle('active');

    var popup = document.getElementById('popup');
    popup.classList.toggle('active');


    this.doctorRequired = "";
    this.dateRequired = "";
  }

  toggle2() {

    var popup2 = document.getElementById('popup2');
    popup2.classList.toggle('active');
  }
  sendRequest(doctorId, description, date) {
    this.openSnackBar("Yêu cầu đang được xử lí ...","")
    this._examination.requestExamination(doctorId, description, date)
      .subscribe(response => {
        console.log("Gui yeu cau thanh cong", response);
        this.openSnackBar("Gửi yêu cầu khám thành công. Quý khách hãy đợi phản hồi của chúng tôi.","Quản lí lịch khám");
        this.noticeRequestSuccess = true;
        let token = this._cookieService.get('access_token');
        this._socketioService.sendRequest(token, doctorId).subscribe();
      },
        err => {
          let temp = err;
          let tempErr = temp.error.message;
          if (tempErr == "Register Medical Examination Existed.") {
            this.openSnackBar("Gửi yêu cầu thất bại. Bạn hãy đặt lịch với bác sĩ khác hoặc đợi yêu cầu gần nhất của bạn được xác nhận.","");
          } else {
            this.openSnackBar("Đã có lỗi hệ thống xảy ra. Chúng tôi rất tiếc vì điều này. \
            Hãy liên hệ cho ban quản trị để thông báo về lỗi. Xin chân thành cảm ơn!","");
            this.noticeRequestSuccess = false;
          }
        });
        this.toggle();
  }

  checkValidTime() {
    var today = new Date();
    var newDate = new Date(this.datePicker);
    if (newDate.getTime() <= today.getTime()) {
      this.isValidDate = false;
    }else{
      this.isValidDate = true;
    }
  }


  changeTime(){
    console.log("change time running");
    this.checkValidTime();
  }


  openSnackBar(message: string, action: string) {
    let snackBarRef = this._snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      duration: 10000,
    });
    snackBarRef.onAction().subscribe(()=>{
      this._router.navigate(["/manage-index/manage-request"]);
    });
  }
}
