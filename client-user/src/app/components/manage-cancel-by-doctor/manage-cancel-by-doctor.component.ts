import { Component, OnInit } from '@angular/core';
import { IMedicalRecord } from 'src/app/entity/medical-record';
import { ExaminationService } from 'src/app/Services/examination.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-manage-cancel-by-doctor',
  templateUrl: './manage-cancel-by-doctor.component.html',
  styleUrls: ['./manage-cancel-by-doctor.component.css']
})
export class ManageCancelByDoctorComponent implements OnInit {
  server_url = environment.SERVER_URL;
  listMedicalExamByDoctor;
  medicalRecord: IMedicalRecord;
  today = new Date();

  constructor(
    private _examination: ExaminationService
  ) { }

  ngOnInit(): void {
    this.getMedicalExaminationByDoctor();
  }

  getMedicalExaminationByDoctor() {
    this._examination.getExaminationByDoctor()
      .subscribe(response => {
        console.log("data medi exam:");
        this.listMedicalExamByDoctor = response;
        this.listMedicalExamByDoctor =  this.listMedicalExamByDoctor.data;
        this.listMedicalExamByDoctor = this.listMedicalExamByDoctor
          .filter(examination => examination.status === 4);
        console.log(this.listMedicalExamByDoctor);
      });
  }

  togglePopup = (type: string, exam) => {
    // if (type === 'close' && !document.getElementById(type + "-" + exam.id).classList.contains("active")) {
    //   this.loadMedicalRecord(exam.id);
    // }
    // else {
      document.getElementById(type + "-" + exam.id).classList.toggle("active");
    // }
  }

  openCloseDialog(examId) { }
}
