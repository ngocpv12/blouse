import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCancelByDoctorComponent } from './manage-cancel-by-doctor.component';

describe('ManageCancelByDoctorComponent', () => {
  let component: ManageCancelByDoctorComponent;
  let fixture: ComponentFixture<ManageCancelByDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageCancelByDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCancelByDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
