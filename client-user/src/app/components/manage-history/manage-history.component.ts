import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RatingChangeEvent } from 'angular-star-rating';
import { ExaminationService } from 'src/app/Services/examination.service';
import { FeedbackService } from 'src/app/Services/feedback.service';
import { MedicalRecordService } from 'src/app/Services/medical-record.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-manage-history',
  templateUrl: './manage-history.component.html',
  styleUrls: ['./manage-history.component.css']
})
export class ManageHistoryComponent implements OnInit {

  constructor(
    private _examinationService: ExaminationService,
    private _feedbackService: FeedbackService,
    private _medicalRecordService: MedicalRecordService,
    private _snackBar: MatSnackBar
  ) { }
  listMedicalExamination;
  momoResponse;
  onRatingChangeResult: RatingChangeEvent;
  server_url = environment.SERVER_URL;
  medicalRecord;
  ngOnInit(): void {
    this.getMedicalExamination();
  }

  getMedicalExamination(){
    this._examinationService.getExaminationByFilter()
      .subscribe(response => {
        console.log("data medi exam:");
        this.listMedicalExamination = response;
        this.listMedicalExamination =  this.listMedicalExamination.data;
        this.listMedicalExamination = this.listMedicalExamination
        .filter(examination => examination.status === 3);
        console.log(this.listMedicalExamination);
      });
  }
  
  loadMedicalRecord(exam) {
    if (exam.status === 3) {
      this._medicalRecordService.getRecordByExamId(exam.id)
      .subscribe(response => {
        this.medicalRecord = response;
        console.log(this.medicalRecord);
        
        document.getElementById("detail-" + exam.id).classList.toggle("active");
      })
    }
    else {
      this.togglePopup(exam, 'detail');
    }
  }

  togglePopup = (exam, type) => {
    document.getElementById(type + "-" + exam.id).classList.toggle("active");
  }

  openSnackBar(message: string, action: string) {
    let snackBarRef = this._snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      duration: 3000,
    });
  }

  sendRating(examId) {
    if (this.onRatingChangeResult) {
      this.togglePopup({ id: examId }, 'rating');
      this._feedbackService.sendRating(examId, this.onRatingChangeResult.rating)
      .subscribe(
        response => {
          this.getMedicalExamination();
          this.openSnackBar(response.message, "");
        }, error => {
          this.openSnackBar(error.message || 'Có lỗi xảy ra khi gửi đánh giá, vui lòng thử lại!', "");
        }
      );
    }
    else {
      this.openSnackBar('Vui lòng chọn số sao để đánh giá', "");
    }
  }

  onRatingChange = ($event: RatingChangeEvent) => {
    console.log('rating change: ', $event);
    this.onRatingChangeResult = $event;
    console.log('rating changed: ', this.onRatingChangeResult);
  }
}