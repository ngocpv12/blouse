import * as decode from 'jwt-decode';
import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/Services/authentication.service';
import { CookieService } from 'ngx-cookie-service';
import { SocialAuthService, SocialUser } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginFailMessage;
  user: SocialUser;
  loggedIn: boolean;
  constructor(
    private fb: FormBuilder,
    private _router: Router,
    private _authenService: AuthenticationService,
    private _cookieService: CookieService,
    private authService: SocialAuthService
  ) { }

  ngOnInit(): void {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      this._authenService.loginWithGoogle(user.name, user.email).subscribe((response)=> {
        window.location.href = "home";
      });
    });
  }
  get username() {
    return this.addForm.get('username');
  }
  get password() {
    return this.addForm.get('password');
  }
  addForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  onSubmit() {
    this._authenService.login(this.addForm.value)
      .subscribe(response => {
        this.loginFailMessage = '';
        window.location.href = "home";
      },
        error => {
          console.log("login fail");
          this.loginFailMessage = 'Tài khoản hoặc mật khẩu không chính xác';
        });
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  checkLoggedIn(){
    return this._cookieService.check('access_token');
  }
}
