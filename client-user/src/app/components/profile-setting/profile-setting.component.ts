import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { UserService } from 'src/app/Services/user.service';
import { CookieService } from 'ngx-cookie-service';
import decode from 'jwt-decode';
import { FormBuilder, Validators } from '@angular/forms';
import { PatientService } from 'src/app/Services/patient.service';
import { environment } from 'src/environments/environment';
import { DoctorService } from 'src/app/Services/doctor.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-profile-setting',
  templateUrl: './profile-setting.component.html',
  styleUrls: ['./profile-setting.component.css']
})
export class ProfileSettingComponent implements OnInit {
  public message: string;
  public imagePath;
  imgURL: any;
  selectedFile: File;
  currentPatient;
  currentDoctor;
  server_url = environment.SERVER_URL;
  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private userService: UserService,
    private cookieService: CookieService,
    private fb: FormBuilder,
    private _patientSerivce: PatientService,
    private _cookieService: CookieService,
    private _doctorService: DoctorService
    ) { }

  ngOnInit(): void {
    const token = this._cookieService.get('access_token');
    const tokenPayload = decode(token);
    if (tokenPayload.role == "PATIENT") {
      this._patientSerivce.getPatientById()
        .subscribe(response => {
          this.currentPatient = response;
          console.log("alolooo");
          console.log(this.currentPatient);
        });
    }else if(tokenPayload.role == "DOCTOR") {
      this._doctorService.getDoctorbyUserId()
        .subscribe(response => {
          this.currentDoctor = response;
          console.log("CURRENT DOCTOR");
          console.log(this.currentDoctor);
        });
    }
  }
  showProfileInfo(){
    this.router.navigate(['profile-info'], {relativeTo: this.route});
  }
  showAccountInfo(){
    this.router.navigate(['account-info'], {relativeTo: this.route});
  }
  showPersonalImage(){
    this.router.navigate(['personal-image'], {relativeTo: this.route});
  }
  onFileChanged(event) {
    this.selectedFile = event.target.files[0];    
  }
  toggle(){
    var blur = document.getElementById('blur');
    blur.classList.toggle('active');
  
    var popup = document.getElementById('popup');
    popup.classList.toggle('active');
  }
  toggle2(){  
    var blur = document.getElementById('blur');
    blur.classList.toggle('active');
    var popup2 = document.getElementById('popup2');
    popup2.classList.toggle('active');
  }
  preview(files) {    
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
    this.selectedFile = files[0];    
  }
  get file() {
    return this.addForm.get('file');
  }

  addForm = this.fb.group({
    file: ['', Validators.required]
  });
  onSubmit(){
    console.log("uploading image");
    let token = this.cookieService.get('access_token');
    let tokenPayload = decode(token); 
    let userId = tokenPayload.id;
    console.log("Selected file in submit");
    console.log(this.selectedFile);
    
    this.userService.uploadImage(userId,this.selectedFile)
      .subscribe(response => {
        console.log(response);
        this.toggle();
        this.toggle2();
      });
  }
  reload(){
    window.location.href = "profile/profile-info";
  }


}
