import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ContactService } from 'src/app/Services/contact.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private contactService: ContactService,
    private _snackBar: MatSnackBar
  ) { }
  get name() {
    return this.addForm.get('name');
  }
  get email() {
    return this.addForm.get('email');
  }
  get phoneNumber() {
    return this.addForm.get('phoneNumber');
  }
  get title() {
    return this.addForm.get('title');
  }
  get message() {
    return this.addForm.get('message');
  }
  ngOnInit(): void {
  }

  addForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', [Validators.required, Validators.pattern('^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$')]],
    phoneNumber: ['', Validators.required],
    title: ['', Validators.required],
    message: ['', Validators.required]
  });
  book() {
    this.router.navigate(['schedule']);
  }
  onSubmit(){
    this.contactService.createContact(this.addForm.value)
      .subscribe(response => {
        console.log(this.addForm.value);
        this.addForm.reset();
        console.log(this.addForm.value);
        console.log(response);
        this.openSnackBar('Tin nhắn của bạn đã được gửi. Chúng tôi sẻ phản hồi bạn sớm nhất.', '');
      });
  }

  openSnackBar(message: string, action: string) {
    let snackBarRef = this._snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      duration: 3000,
    });
  }
}
