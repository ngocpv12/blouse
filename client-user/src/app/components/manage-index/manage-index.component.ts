import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ExaminationService } from 'src/app/Services/examination.service';
import decode from 'jwt-decode';
@Component({
  selector: 'app-manage-index',
  templateUrl: './manage-index.component.html',
  styleUrls: ['./manage-index.component.css']
})
export class ManageIndexComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _examination: ExaminationService,
    private _cookieService: CookieService
  ) { }

  currentUrl;
  currentIndex;
  listMedicalExamination;
  // lstRequestExamination;
  // lstWfpExamination;
  // lstScheduleExamination;
  // lstHistoryExamination;
  // lstCancelExamination;
  role: string;
  ngOnInit(): void {
    const token = this._cookieService.get('access_token');
    const tokenPayload = decode(token);
    if(tokenPayload.role == "DOCTOR"){
      this.role = "DOCTOR";
    }else if(tokenPayload.role == "PATIENT"){
      this.role = "PATIENT"
    }
    console.log("ROLE:");
    console.log(this.role);
    console.log("What is this router");
    console.log(this.router.url);
    this.currentUrl = this.router.url;
    if (this.role === 'PATIENT') {
      if (this.currentUrl == '/manage-index/manage-all') {
        this.currentIndex = 'manage-all';
      } else if (this.currentUrl == '/manage-index/manage-request') {
        this.currentIndex = 'manage-request';
      } else if (this.currentUrl == '/manage-index/manage-confirmed') {
        this.currentIndex = 'manage-confirmed';
      } else if (this.currentUrl == '/manage-index/manage-schedule') {
        this.currentIndex = 'manage-schedule';
      } else if (this.currentUrl == '/manage-index/manage-history') {
        this.currentIndex = 'manage-history';
      } else if (this.currentUrl == '/manage-index/manage-cancel') {
        this.currentIndex = 'manage-cancel';
      }
    }
    else {
      if (this.currentUrl == '/manage-index/manage-all') {
        this.currentIndex = 'manage-all-by-doctor';
      } else if (this.currentUrl == '/manage-index/manage-confirmed') {
        this.currentIndex = 'manage-confirmed-by-doctor';
      } else if (this.currentUrl == '/manage-index/manage-history') {
        this.currentIndex = 'manage-history-by-doctor';
      } else if (this.currentUrl == '/manage-index/manage-cancel') {
        this.currentIndex = 'manage-cancel-by-doctor';
      }
    }
  }

  showAll() {
    if (this.role === 'PATIENT') {
      this.router.navigate(['manage-all'], { relativeTo: this.route });
      this.currentIndex = "manage-all";
    }
    else {
      this.router.navigate(['manage-all-by-doctor'], { relativeTo: this.route });
      this.currentIndex = "manage-all-by-doctor";
    }
  }
  showRequest() {
    this.router.navigate(['manage-request'], { relativeTo: this.route });
    this.currentIndex = "manage-request";
  }
  showConfirmed() {
    if (this.role === 'PATIENT') {
      this.router.navigate(['manage-confirmed'], { relativeTo: this.route });
      this.currentIndex = "manage-confirmed";
    }
    else {
      this.router.navigate(['manage-confirmed-by-doctor'], { relativeTo: this.route });
      this.currentIndex = "manage-confirmed-by-doctor";
    }
  }
  showSchedule() {
    this.router.navigate(['manage-schedule'], { relativeTo: this.route });
    this.currentIndex = "manage-schedule";
  }
  showHistory() {
    if (this.role === 'PATIENT') {
      this.router.navigate(['manage-history'], { relativeTo: this.route });
      this.currentIndex = "manage-history";
    }
    else {
      this.router.navigate(['manage-history-by-doctor'], { relativeTo: this.route });
      this.currentIndex = "manage-history-by-doctor";
    }
  }
  showCanceled() {
    if (this.role === 'PATIENT') {
      this.router.navigate(['manage-cancel'], { relativeTo: this.route });
      this.currentIndex = "manage-cancel";
    }
    else {
      this.router.navigate(['manage-cancel-by-doctor'], { relativeTo: this.route });
      this.currentIndex = "manage-cancel-by-doctor";
    }
  }
}
