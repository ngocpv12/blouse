import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ExaminationService } from 'src/app/Services/examination.service';
import { MedicalRecordService } from 'src/app/Services/medical-record.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-manage-request',
  templateUrl: './manage-request.component.html',
  styleUrls: ['./manage-request.component.css']
})
export class ManageRequestComponent implements OnInit {
  constructor(
    private _examinationService: ExaminationService,
    private _medicalRecordService: MedicalRecordService,
    private _snackBar: MatSnackBar
  ) { }
  listMedicalExamination;
  server_url = environment.SERVER_URL;
  medicalRecord;
  ngOnInit(): void {
    this.getMedicalExamination();
  }

  getMedicalExamination(){
    this._examinationService.getExaminationByFilter()
      .subscribe(response => {
        console.log("data medi exam:");
        this.listMedicalExamination = response;
        this.listMedicalExamination =  this.listMedicalExamination.data;
        this.listMedicalExamination = this.listMedicalExamination
          .filter(examination => examination.status === 0);
        console.log(this.listMedicalExamination);
      });
  }

  cancelExamination(examId) {
    this._examinationService.cancelExamination({ id: examId })
    .subscribe(
      response => {
        this.getMedicalExamination();
        this.openSnackBar(response.message, "");
      }, error => {
        this.openSnackBar(error.message, "");
      }
    );
  }

  loadMedicalRecord(exam) {
    if (exam.status === 3) {
      this._medicalRecordService.getRecordByExamId(exam.id)
      .subscribe(response => {
        this.medicalRecord = response;
        console.log(this.medicalRecord);
        
        document.getElementById("detail-" + exam.id).classList.toggle("active");
      })
    }
    else {
      this.togglePopup(exam, 'detail');
    }
  }

  togglePopup = (exam, type) => {
    document.getElementById(type + "-" + exam.id).classList.toggle("active");
  }

  openSnackBar(message: string, action: string) {
    let snackBarRef = this._snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      duration: 3000,
    });
  }
}
