import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { ExaminationService } from 'src/app/Services/examination.service';
import { MedicalRecordService } from 'src/app/Services/medical-record.service';
import { PaymentService } from 'src/app/Services/payment.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-manage-schedule',
  templateUrl: './manage-schedule.component.html',
  styleUrls: ['./manage-schedule.component.css']
})
export class ManageScheduleComponent implements OnInit {

  constructor(
    private _examination: ExaminationService,
    private _medicalRecordService: MedicalRecordService,
    private _paymentService: PaymentService,
    private _route: ActivatedRoute,
    private router: Router,
    private _snackBar: MatSnackBar
  ) { }  
  listMedicalExamination;
  medicalRecord;
  server_url = environment.SERVER_URL;
  paymentInfo
  ngOnInit(): void {
    this.getQueryParam();
    this.getMedicalExamination();
  }

  getMedicalExamination(){
    this._examination.getExaminationByFilter()
      .subscribe(response => {
        console.log("data medi exam:");
        this.listMedicalExamination = response;
        this.listMedicalExamination =  this.listMedicalExamination.data;
        this.listMedicalExamination = this.listMedicalExamination
        .filter(examination => examination.status === 2);
        console.log(this.listMedicalExamination);
      });
  }

  loadMedicalRecord(exam) {
    if (exam.status === 3) {
      this._medicalRecordService.getRecordByExamId(exam.id)
      .subscribe(response => {
        this.medicalRecord = response;
        console.log(this.medicalRecord);
        
        document.getElementById("detail-" + exam.id).classList.toggle("active");
      })
    }
    else {
      this.togglePopup(exam, 'detail');
    }
  }

  togglePopup = (exam, type) => {
    document.getElementById(type + "-" + exam.id).classList.toggle("active");
  }

  getQueryParam() {
    this._route.queryParams.subscribe((params: ParamMap) => {
      if (params['partnerCode'] == 'MOMOPAVE20200801' && params['accessKey'] == '83C3ypFVQ8lKXWHw') {
        this.paymentInfo = {
          partnerCode: params['partnerCode'],
          accessKey: params['accessKey'],
          requestId: params['requestId'],
          amount: params['amount'],
          orderId: params['orderId'],
          orderInfo: params['orderInfo'],
          orderType: params['orderType'],
          transId: params['transId'],
          errorCode: params['errorCode'],
          message: params['message'],
          localMessage: params['localMessage'],
          responseTime: params['responseTime'],
          signature: params['signature'],
          extraData: params['extraData'],
          payType: params['payType'],
        };
        this.sendNotification(this.paymentInfo);
      }
    });
  }

  sendNotification = (data) => {
    this._paymentService.confirmPayment(data)
    .subscribe(
      response => {
        console.log('Success!', response);
        this.router.navigate(['/manage-index/manage-schedule']);
        this.openSnackBar(response.message, null);
      }, error => {
        console.error('Error!', error);
        this.openSnackBar(error.error.message, null);
      }
    );
  }

  openSnackBar(message: string, action: string) {
    let snackBarRef = this._snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      duration: 3000,
    });
  }
}
