import decode from 'jwt-decode';
import { SocketioService } from 'src/app/Services/socketio.service';
import { CookieService } from 'ngx-cookie-service';
import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'HomePage';
  notifications: string[] = [];
  private subscription;
  private tokenPayload;

  constructor(
    private _cookieService: CookieService,
    private _socketioService: SocketioService,
    private _router: Router,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    if (this._cookieService.check('access_token')) {
      let token = this._cookieService.get('access_token');
      this.subscription = this._socketioService.getNotification(token).subscribe(notification =>{
        this.notifications.push(notification);
        this.openSnackBar(this.notifications[this.notifications.length -1], 'Xem');
      });
      this.tokenPayload = decode(token);
    }
  }
  
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  
  openSnackBar(message: string, action: string) {
    let snackBarRef = this._snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      duration: 5000,
    });
    snackBarRef.onAction().subscribe(() => {
      if (this.tokenPayload.role === "PATIENT") {
        this._router.navigate(["/manage-index/manage-confirmed"]);
      }
      else {
        this._router.navigate(["/manage-index/manage-confirmed-by-doctor"]);
      }
    });
  }
}
