import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageAllByDoctorComponent } from './manage-all-by-doctor.component';

describe('ManageAllByDoctorComponent', () => {
  let component: ManageAllByDoctorComponent;
  let fixture: ComponentFixture<ManageAllByDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageAllByDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageAllByDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
