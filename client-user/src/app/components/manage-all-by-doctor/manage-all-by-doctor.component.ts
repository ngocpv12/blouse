import { CloseExaminationFormComponent } from './../general-components/close-examination-form/close-examination-form.component';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IMedicalRecord } from 'src/app/entity/medical-record';
import { ExaminationService } from 'src/app/Services/examination.service';
import { MedicalRecordService } from 'src/app/Services/medical-record.service';
import { environment } from 'src/environments/environment';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-manage-all-by-doctor',
  templateUrl: './manage-all-by-doctor.component.html',
  styleUrls: ['./manage-all-by-doctor.component.css']
})
export class ManageAllByDoctorComponent implements OnInit {

  constructor(
    private _examination: ExaminationService,
    private _medicalRecordService: MedicalRecordService,
    private _dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { }

  server_url = environment.SERVER_URL;
  listMedicalExamByDoctor;
  medicalRecord: IMedicalRecord;
  today = new Date();
  
  ngOnInit(): void {
    this.getMedicalExaminationByDoctor();
  }

  getMedicalExaminationByDoctor() {
    this._examination.getExaminationByDoctor()
      .subscribe(response => {
        console.log("data medi exam:");
        this.listMedicalExamByDoctor = response;
        this.listMedicalExamByDoctor =  this.listMedicalExamByDoctor.data;
        console.log(this.listMedicalExamByDoctor);
      });
  }

  loadMedicalRecord(exam) {
    if (exam.status === 3) {
      this._medicalRecordService.getRecordByExamId(exam.id)
      .subscribe(response => {
        this.medicalRecord = response;
        console.log(this.medicalRecord);
        
        document.getElementById("detail-" + exam.id).classList.toggle("active");
      })
    }
    else {
      this.togglePopup('detail', exam);
    }
  }

  togglePopup = (type: string, exam) => {
      document.getElementById(type + "-" + exam.id).classList.toggle("active");
  }

  openCloseDialog(examId) {
    let dialogRef = this._dialog.open(CloseExaminationFormComponent, {
      data: {
        examId: examId
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.openSnackBar(result.message, null);
        this.getMedicalExaminationByDoctor();
        }
    })
  }

  openSnackBar(message: string, action: string) {
    let snackBarRef = this._snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      duration: 3000,
    });
  }
  
  compareDate(rawDate1, rawDate2) {
    let d1 = new Date(rawDate1);
    let d2 = new Date(rawDate2);

    return d1.setHours(0, 0, 0, 0) >= d2.setHours(0, 0, 0, 0);
  }
}
//1GjmV8CV