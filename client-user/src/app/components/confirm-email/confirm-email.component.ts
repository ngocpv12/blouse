import { UserService } from 'src/app/Services/user.service';
import { environment } from 'src/environments/environment';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.css']
})
export class ConfirmEmailComponent implements OnInit {
  BLOUSE_URL = environment.BLOUSEVN;
  LOGIN_URL = this.BLOUSE_URL + 'login';
  token;
  success = false;

  constructor(
    private route: ActivatedRoute,
    private _userService: UserService,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.token = params['token'];
    });
  }

  sendToken(token) {
    this._userService.verifyUser(token)
    .subscribe(response => {
      this.success = true;
      console.log(response);
    }, error => {
      this.success = false;
      console.log(error);
    });
  }

  returnToLogin() {
    this._router.navigate(['/login']);
  }
}
