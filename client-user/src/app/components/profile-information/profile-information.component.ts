import { Component, OnInit } from '@angular/core';
import { PatientService } from 'src/app/Services/patient.service';
import { IPatient } from 'src/app/entity/patient';
import { Validators, FormBuilder } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import decode from 'jwt-decode';
import { DoctorService } from 'src/app/Services/doctor.service';
@Component({
  selector: 'app-profile-information',
  templateUrl: './profile-information.component.html',
  styleUrls: ['./profile-information.component.css']
})
export class ProfileInformationComponent implements OnInit {
  currentPatient;
  userPatient: IPatient;
  fullName;
  gender;
  phoneNumber;
  dateOfBirth;
  address;
  mail;
  currentDoctor;
  constructor(
    private _patientSerivce: PatientService,
    private _cookieService: CookieService,
    private _doctorService: DoctorService
    ) { }

  ngOnInit(): void {
    const token = this._cookieService.get('access_token');
    const tokenPayload = decode(token);
    if (tokenPayload.role == "PATIENT") {
      this._patientSerivce.getPatientById()
        .subscribe(response => {
          this.currentPatient = response;
          this.gender = this.currentPatient.user.gender == 1 ? "1":"2";
          console.log(this.currentPatient);
        });
    }else if(tokenPayload.role == "DOCTOR") {
      this._doctorService.getDoctorbyUserId()
        .subscribe(response => {
          this.currentDoctor = response;
          this.gender = this.currentDoctor.user.gender == 1 ? "1":"2";
          console.log("GENDER");
          console.log(this.gender);
          
          
          console.log("CURRENT DOCTOR");
          console.log(this.currentDoctor);
        });
    }  

  }

  onSubmit(fullName,gender,phoneNumber, dateOfBirth, address){
    let token = this._cookieService.get('access_token');
    let tokenPayload = decode(token);
    let userId = tokenPayload.id;
    this._patientSerivce.update(userId,fullName,gender,phoneNumber, dateOfBirth, address)
      .subscribe(response => {
        console.log("Update successfully", response);
        this.toggle3();
      }, 
      err => {
        console.log("error", err);
      })
  }
  toggle3(){
    var popup3 = document.getElementById('popup3');
    popup3.classList.toggle('active');
  }
}
