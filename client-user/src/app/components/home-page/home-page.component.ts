import { Component, OnInit } from '@angular/core';
import { DoctorService } from 'src/app/Services/doctor.service';
import { IDoctor } from 'src/app/entity/doctor';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  constructor(
    private doctor: DoctorService, 
    private _router: Router
    ) { }
  top3Doctors;
  server_url = environment.SERVER_URL;
  ngOnInit(): void {
    this.load3Doctors();
  }
  load3Doctors(){
    console.log("Load 3 doctor");
    
    this.doctor.loadThreeDoctor().subscribe(response => {
      this.top3Doctors = response;
      console.log("Data top 3");
      console.log(this.top3Doctors);
      
    });
  }

  onSelect(doctor: IDoctor){

    this._router.navigate(['/doctor', doctor.id,{
      doctorId: doctor.id,
      name: doctor.user.fullName,
      departmentName: doctor.department.name,
      hospitalName: doctor.department.hospital.name,
      image:  this.server_url + doctor.image
    }]);
  }
}
