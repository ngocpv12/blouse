import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AuthenticationService } from 'src/app/Services/authentication.service';
import { DoctorService } from 'src/app/Services/doctor.service';
import { PatientService } from 'src/app/Services/patient.service';
import decode from 'jwt-decode';
@Component({
  selector: 'app-account-info',
  templateUrl: './account-info.component.html',
  styleUrls: ['./account-info.component.css']
})
export class AccountInfoComponent implements OnInit {
  loginStatus;
  currentPatient;
  currentDoctor;
  changeSuccess:string;
  changeFail:string;
  constructor(
    private _patientSerivce: PatientService,
    private _authService: AuthenticationService,
    private _cookieService: CookieService,
    private _doctorService: DoctorService
    ) { }

  ngOnInit(): void {
    const token = this._cookieService.get('access_token');
    const tokenPayload = decode(token);
    if (tokenPayload.role == "PATIENT") {
      this._patientSerivce.getPatientById()
        .subscribe(response => {
          this.currentPatient = response;
          console.log("alolooo");
          console.log(this.currentPatient);
        });
    }else if(tokenPayload.role == "DOCTOR") {
      this._doctorService.getDoctorbyUserId()
        .subscribe(response => {
          this.currentDoctor = response;
          console.log("CURRENT DOCTOR");
          console.log(this.currentDoctor);
        });
    }
  }


  toggleChangePassword() {
    var blur = document.getElementById('blur2');
    blur.classList.toggle('active');

    var popupChangePass = document.getElementById('popupChangePass');
    popupChangePass.classList.toggle('active');
  }
  toggleChangeNotice() {
    var blur = document.getElementById('blur2');
    blur.classList.toggle('active');

    var changeNotice = document.getElementById('changeNotice');
    changeNotice.classList.toggle('active');
  }
  confirm(newPassword, oldPassword){
    this._authService.updatePassword(oldPassword,newPassword)
      .subscribe(response => {
        console.log(response);
        this.changeSuccess = "Đổi mật khẩu thành công!";
        this.changeFail = "";
        this.toggleChangePassword();
        this.toggleChangeNotice();
      },
      err => {
        console.log(err);
        this.toggleChangePassword();
        this.toggleChangeNotice();
        this.changeSuccess = "";
        this.changeFail = "Đổi mật khẩu thất bại. Đã có lỗi xảy ra :("
      });
  }
  logout(){
    this._authService.logout();
    this.loginStatus = false;
    // this.signOut();
    console.log("log out successfully!");
    window.location.href = "login";
  }
}
