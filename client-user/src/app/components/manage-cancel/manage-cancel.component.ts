import { Component, OnInit } from '@angular/core';
import { ExaminationService } from 'src/app/Services/examination.service';
import { MedicalRecordService } from 'src/app/Services/medical-record.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-manage-cancel',
  templateUrl: './manage-cancel.component.html',
  styleUrls: ['./manage-cancel.component.css']
})
export class ManageCancelComponent implements OnInit {

  constructor(
    private _examination: ExaminationService,
    private _medicalRecordService: MedicalRecordService,
  ) { }
  listMedicalExamination;
  medicalRecord
  ngOnInit(): void {
    this.getMedicalExamination();
  }
  server_url = environment.SERVER_URL;
  getMedicalExamination(){
    this._examination.getExaminationByFilter()
      .subscribe(response => {
        console.log("data medi exam:");
        this.listMedicalExamination = response;
        this.listMedicalExamination =  this.listMedicalExamination.data;
        this.listMedicalExamination = this.listMedicalExamination
          .filter(examination => examination.status === 4);
        console.log(this.listMedicalExamination);
      });
  }

  

  loadMedicalRecord(exam) {
    if (exam.status === 3) {
      this._medicalRecordService.getRecordByExamId(exam.id)
      .subscribe(response => {        
        this.medicalRecord = response;
        console.log(this.medicalRecord);
        
        document.getElementById("detail-" + exam.id).classList.toggle("active");
      })
    }
    else {
      this.togglePopup(exam, 'detail');
    }
  }

  togglePopup = (exam, type) => {
    document.getElementById(type + "-" + exam.id).classList.toggle("active");
  }
}