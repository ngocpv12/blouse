import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageHistoryByDoctorComponent } from './manage-history-by-doctor.component';

describe('ManageHistoryByDoctorComponent', () => {
  let component: ManageHistoryByDoctorComponent;
  let fixture: ComponentFixture<ManageHistoryByDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageHistoryByDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageHistoryByDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
