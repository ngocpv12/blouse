import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { IMedicalRecord } from 'src/app/entity/medical-record';
import { ExaminationService } from 'src/app/Services/examination.service';
import { MedicalRecordService } from 'src/app/Services/medical-record.service';
import { environment } from 'src/environments/environment';
import { CloseExaminationFormComponent } from '../general-components/close-examination-form/close-examination-form.component';

@Component({
  selector: 'app-manage-history-by-doctor',
  templateUrl: './manage-history-by-doctor.component.html',
  styleUrls: ['./manage-history-by-doctor.component.css']
})
export class ManageHistoryByDoctorComponent implements OnInit {
  server_url = environment.SERVER_URL;
  listMedicalExamByDoctor;
  medicalRecord;
  today = new Date();

  constructor(
    private _examination: ExaminationService,
    private _medicalRecordService: MedicalRecordService,
    private _dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getMedicalExaminationByDoctor();
  }

  getMedicalExaminationByDoctor() {
    this._examination.getExaminationByDoctor()
      .subscribe(response => {
        console.log("data medi exam:");
        this.listMedicalExamByDoctor = response;
        this.listMedicalExamByDoctor =  this.listMedicalExamByDoctor.data;
        this.listMedicalExamByDoctor = this.listMedicalExamByDoctor
        .filter(examination => examination.status === 3);
        console.log(this.listMedicalExamByDoctor);
      });
  }

  loadMedicalRecord(examId) {
    this._medicalRecordService.getRecordByExamId(examId)
    .subscribe(response => {
      this.medicalRecord = response;
      console.log(this.medicalRecord);
      
      document.getElementById("detail-" + examId).classList.toggle("active");
    })
  }

  togglePopup = (type: string, exam) => {
    // if (type === 'close' && !document.getElementById(type + "-" + exam.id).classList.contains("active")) {
    //   this.loadMedicalRecord(exam.id);
    // }
    // else {
      document.getElementById(type + "-" + exam.id).classList.toggle("active");
    // }
  }

  openCloseDialog(examId) {
    let dialogRef = this._dialog.open(CloseExaminationFormComponent, {
      data: {
        examId: examId
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getMedicalExaminationByDoctor();
    })
  }
}
