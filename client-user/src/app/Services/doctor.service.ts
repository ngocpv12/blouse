import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IDoctor } from '../entity/doctor';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';
import decode from 'jwt-decode';
@Injectable({
  providedIn: 'root'
})
export class DoctorService {
  constructor(private _http: HttpClient, private _cookieService: CookieService) { }
  // _url = '../assets/fixedData/record.json';
  //_url = 'http://localhost:3001/entity';
  _url = environment.SERVER_URL + 'doctor';

  getAllDoctors(page,size): Observable<IDoctor[]> {
    return this._http.get<IDoctor[]>(this._url+ '?page=' + page + '&size=' + size)
      .pipe(catchError(this.errorHandler));
  }

  getDoctorbyUserId(): Observable<IDoctor[]> {
    let token = this._cookieService.get('access_token');
    let tokenPayload = decode(token);
    let id = tokenPayload.id;
    return this._http.get<IDoctor[]>(this._url + '/user/' + id)
      .pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "Server Error");
  }

  updateDoctor(id, userData) {
    return this._http.put<any>(this._url + '/' + id, userData);
  }

  // Get doc tor by hospital department or doctor name
  getDoctorByFilter(page,size,hospitalId: number, departmentId: number, txtSearch: string): Observable<IDoctor[]> {
    let searchUrl = this._url + '/search?page='+ page + '&size='+ size+'&';
    if (hospitalId != null) {
      searchUrl = searchUrl + 'hospitalId=' + hospitalId + '&';
    }
    if (departmentId != null) {
      searchUrl = searchUrl + 'departmentId=' + departmentId + '&';
    }
    if (txtSearch != null) {
      searchUrl = searchUrl + 'txtSearch=' + txtSearch + '&';
    }

    return this._http.get<IDoctor[]>(searchUrl)
      .pipe(catchError(this.errorHandler));
  }

  loadThreeDoctor(){
    return this._http.get<IDoctor[]>(this._url + "/top-three")
    .pipe(catchError(this.errorHandler));
  }
  uploadImage(fileImage: File){
    let formData = new FormData();
    formData.append('d-image', fileImage);
    return this._http.post<any>(this._url + "/upload-image",formData)
      .pipe(catchError(this.errorHandler));
  }
}
