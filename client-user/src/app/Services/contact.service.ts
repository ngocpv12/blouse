import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  _url = environment.SERVER_URL + 'contact';
  constructor(
    private http: HttpClient
  ) { }
  createContact(formData){
    return this.http.post<any>(this._url, formData);
  }
}
