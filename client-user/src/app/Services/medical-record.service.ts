import { IMedicalRecord } from './../entity/medical-record';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MedicalRecordService {
  _url = environment.SERVER_URL + 'medical-record/';

  constructor(
    private _http: HttpClient
  ) { }

  getRecordByExamId = (examId) => {
    return this._http.get<IMedicalRecord>(this._url + 'by-exam/' + examId);
  }
}
