import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  _url = environment.SERVER_URL + 'user';
  constructor(private _http: HttpClient) { }
  uploadImage(id,fileImage: File){
    let formData = new FormData();
    formData.append('id', id);
    formData.append('avatar', fileImage);
    return this._http.post<any>(this._url + "/upload-avatar",formData)
      .pipe(catchError(this.errorHandler));
  }

  verifyUser(token) {
    return this._http.get<any>(this._url + '/verify-user/' + token);
  }
  
  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "Server Error");
  }
}
