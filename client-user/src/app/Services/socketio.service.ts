import decode from 'jwt-decode';
import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketioService {
  constructor() { }

  private url = environment.SOCKET_ENDPOINT;  
  private socket;
  
  getNotification(token): Observable<string> {
    let observable = new Observable<string>(observer => {
      let tokenPayload = decode(token);
      this.socket = io(this.url);

      if(tokenPayload.role === "PATIENT") {
        this.socket.emit("userLogin", {
          "userId": tokenPayload.id
        });
      }
      else if(tokenPayload.role === "DOCTOR") {
        this.socket.emit("doctorLogin", {
          "doctorId": tokenPayload.id
        });
      }

      this.socket.on('confirmed', (message) => {
        let notification: string = message;
        observer.next(notification);
      });

      this.socket.on('newRequest', (message) => {
        let notification: string = message;
        observer.next(notification);
      });

      return () => {
        this.socket.disconnect();
      };
    })
    return observable;
  }

  sendRequest(token, doctorId): Observable<any> {
    let observable = new Observable<any>(observer => {
      let tokenPayload = decode(token);
      if(tokenPayload.role === "PATIENT") {
        this.socket = io(this.url);
        this.socket.emit("sendRequest", {
          "userId": tokenPayload.id,
          "doctorId": doctorId
        });
      }
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
}
