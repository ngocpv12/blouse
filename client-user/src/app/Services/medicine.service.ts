import { IMedicine } from './../entity/medicine';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MedicineService {
  _url = environment.SERVER_URL + 'medicine/';

  constructor(
    private _http: HttpClient
  ) { }

  searchMedicine = (text: string) => {
    return this._http.get<IMedicine[]>(this._url + 'search?q=' + text);
  }
}
