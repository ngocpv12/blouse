import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';
import decode from 'jwt-decode';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IMedicalExamination } from '../entity/medical-examination';

@Injectable({
  providedIn: 'root'
})
export class ExaminationService {

  constructor(
    private _http: HttpClient,
    private _cookieService: CookieService
  ) { }
  
  _url = environment.SERVER_URL + 'medical-examination';

  requestExamination(doctorId,description, date){
    return this._http.post<any>(this._url, {doctorId: doctorId, appointmentTime: date, description: description});
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "Server Error");
  }

  getAllRequestExamination(){
    return this._http.get<IMedicalExamination[]>(this._url)
    .pipe(catchError(this.errorHandler));
  }
  
  //p = patientId
  getExaminationByFilter(): Observable<IMedicalExamination[]> {
    let token = this._cookieService.get('access_token');
    let tokenPayload = decode(token);
    let patientId = tokenPayload.id;
  
    let searchUrl = this._url + '?p=' + patientId;    

    return this._http.get<IMedicalExamination[]>(searchUrl)
      .pipe(catchError(this.errorHandler));
  }

  getExaminationByDoctor(): Observable<IMedicalExamination[]> {
    let searchUrl = this._url + '/by-doctor';    

    return this._http.get<IMedicalExamination[]>(searchUrl)
      .pipe(catchError(this.errorHandler));
  }

  closeExamination(examId, medicalRecord) {
    return this._http.put<any>(this._url + '/close/' + examId, medicalRecord);
  }

  cancelExamination(body) {
    return this._http.put<any>(this._url + '/cancel', body);
  }
}
