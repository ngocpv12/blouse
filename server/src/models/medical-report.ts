import { MedicalExamination } from './medical-examination';
import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany, OneToOne } from 'typeorm';
import { Patient } from './patient';

@Entity('medical_report')
export class MedicalReport extends BaseEntity {
    @PrimaryGeneratedColumn({
        type: 'int',
        name: 'id'
    })
    id: number;

    @ManyToOne(type => Patient, patient => patient.medicalReports)
    @JoinColumn({ name: 'patient_id' })
    patient: Patient;
    @Column({ name: 'patient_id' })
    patientId: string

    @OneToOne(type => MedicalExamination, medicalExamination => medicalExamination.medicalReport)
    @JoinColumn({ name: 'medical_examination_id' })
    medicalExamination: MedicalExamination;
    @Column({ name: 'medical_examination_id' })
    medicalExaminationId: number;

    @Column({
        type: 'text',
        default: null,
        name: 'description'
    })
    description: string;

    @Column({
        type: 'tinyint',
        width: 1,
        name: 'is_active',
        default: 1
    })
    isActive: number;

    @Column({
        type: 'datetime',
        name: 'created_at',
    })
    createdAt: Date;

    @Column({
        type: 'datetime',
        name: 'modified_at',
    })
    modifiedAt: Date;
}