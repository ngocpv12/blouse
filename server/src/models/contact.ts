import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { Length, MaxLength, IsEmail , IsPhoneNumber} from 'class-validator';
import { Admin } from './admin';

@Entity('contact')
export class Contact extends BaseEntity {
    @PrimaryGeneratedColumn({
        type: 'int',
        name: 'id'
    })
    id: number;

    @Column({
        type: 'varchar',
        length: '255',
        name: 'name'
    })
    @Length(1, 255)
    name: string;

    @Column({
        type: 'varchar',
        length: 255,
        name: 'email'
    })
    @MaxLength(255)
    @IsEmail()
    email: string;

    @Column({
        type: 'varchar',
        length: 20,
        name: 'phoneNumber'
    })
    @MaxLength(20)
    @IsPhoneNumber('VN', {
        message: 'Số điện thoại của bạn không hợp lệ!'
    })
    phoneNumber: string;

    @Column({
        type: 'varchar',
        length: '255',
        name: 'title'
    })
    @Length(1, 255)
    title: string;

    @Column({
        type: 'text',
        name: 'message'
    })
    @MaxLength(1000)
    message: string;

    @ManyToOne(type => Admin, admin => admin.contacts)
    @JoinColumn({ name: 'respondent_id'})
    admin: Admin;
    @Column({ name: 'respondent_id', default: null })
    respondentId: string

    @Column({
        type: 'text',
        name: 'reply'
    })
    @MaxLength(1000)
    reply: string;

    @Column({
        type: 'tinyint',
        width: 1,
        name: 'is_active',
        default: 1
    })
    isActive: number;

    @Column({
        type: 'datetime',
        name: 'created_at',
    })
    createdAt: Date;

    @Column({
        type: 'datetime',
        name: 'modified_at',
    })
    modifiedAt: Date;

}