import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import multer from 'multer';
import * as fs from 'fs';
import { User } from "../models/user";

class Common {
    static getCurrentUserId = (req: Request) => {
        let token = <string>req.headers["access_token"];
        if (!token) return null;
        try {
            const { id } = <any>jwt.verify(token, process.env.JWT_SECRET || '@Hung123');
            return id;
        } catch (error) {
            return null;
        }
    }

    static storage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, './uploads/user/avatar/');
        },
        filename: (req, file, cb) => {
            cb(null, req.body.id + '.png');
        }
    });

    static upload = multer({ storage: Common.storage });

    static excelFilter = (req, file, cb) => {
        if (
          file.mimetype.includes("excel") ||
          file.mimetype.includes("spreadsheetml")
        ) {
          cb(null, true);
        } else {
          cb("Please upload only excel file.", false);
        }
    };

    static excelStorage = multer.diskStorage({
        destination: (req, file, cb) => {
            let dir;
            let id = Common.getCurrentUserId(req);
            if (req.body.role === 'STAFF') {
                dir = './uploads/staff/' + id + '/';
            }
            else {
                dir = './uploads/admin/' + id + '/';
            }
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }
            cb(null, './uploads/staff/' + id + '/');
        },
        filename: (req, file, cb) => {
            cb(null, `${Date.now()}-blouse-${file.originalname}`);
        }
    });

    static uploadExcel = multer({ storage: Common.excelStorage, fileFilter: Common.excelFilter });

    static doctorImageStorage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, './uploads/user/doctor/image');
        },
        filename: (req, file, cb) => {
            let userId = Common.getCurrentUserId(req);
            cb(null, `${Date.now()}-${userId}-${file.originalname}`);
        }
    })

    static uploadDoctorImage = multer({ storage: Common.doctorImageStorage });

    static minifyString = (s: string) => {
        return s.normalize('NFD')
            .replace(/[\u0300-\u036f]/g, '')
            .replace(/[đĐ]/g, m => m === 'đ' ? 'd' : 'D')
            .replace(/ + /g," ")
            .trim()
            .toLowerCase();
    }
}

export default Common;
