import { Doctor } from './../models/doctor';
import { Patient } from './../models/patient';
import { getRepository } from 'typeorm';
import * as jwt from "jsonwebtoken";
import { Request, Response } from "express";
import { User } from "../models/user";
import { Constant } from "../utils/constant";
import MailService from "../services/mail-service";
const generator = require('generate-password');
import { v4 as uuid } from 'uuid';
import * as bcrypt from 'bcryptjs';
const saltRounds = 10;
class UserService {

    static registerUser = async (req: Request, res: Response, userId: string, userRole: string) => {
        let user = await User.findOne({
            where: [
                { username: req.body.username },
                { phoneNumber: req.body.phoneNumber },
                { email: req.body.email }]
        });
        if (user) return false;
        let hashed = bcrypt.hashSync(req.body.password, saltRounds);

        user = new User();
        user.id = userId;
        user.phoneNumber = req.body.phoneNumber;
        user.email = req.body.email;
        user.username = req.body.username;
        user.password = hashed;
        user.avatar = process.env.AVATAR_DEFAULT || 'uploads/user/avatar/avatar_default.png'
        user.role = userRole;
        user.isVerified = Constant.ZERO;
        user.createdAt = (new Date());
        user.modifiedAt = (new Date());
        await User.save(user);

        const token = jwt.sign({ id: user.id, role: user.role }, process.env.JWT_SECRET || '@Hung123', { expiresIn: "1h" });

        let infor = MailService.sendEmail(req, res, token);
        if (!infor) return false;

        return true;
    };

    static verifyUser = async (req: Request, res: Response) => {
        const token = req.params.token;

        if (!token) return res.status(400).send({ message: 'Invalid token!' });

        try {
            //Validate the token
            const decoded = <any>jwt.verify(token, process.env.JWT_SECRET);
            res.locals.jwtPayload = decoded;

            const { id, role } = decoded;
            const user = await User.findOne(id);
            if (user.isVerified) return res.status(400).send("Account Had Verified.");

            user.isVerified = Constant.ONE;
            user.modifiedAt = (new Date());
            await User.save(user);

            return res.status(200).send({ message: "Verify User Successfully !" });
        } catch (error) {
            return res.status(400).send({ message: 'Incorrect or expired link' });
        }
    };

    static createUser = async (req: Request, res: Response, userId: string, userRole: string) => {
        let user = await User.findOne({
            where: [
                { username: req.body.username },
                { phoneNumber: req.body.phoneNumber },
                { email: req.body.email }]
        });
        if (user) return false;
        // Generate password
        let password = generator.generate({
            length: 8,
            numbers: true
        });
        let hashed = bcrypt.hashSync(password, saltRounds);

        user = new User();
        user.id = userId;
        user.fullName = req.body.fullName;
        user.title = req.body.title;
        user.gender = req.body.gender;
        user.phoneNumber = req.body.phoneNumber;
        user.dateOfBirth = req.body.dateOfBirth;
        user.address = req.body.address;
        user.email = req.body.email;
        user.username = req.body.username;
        user.password = hashed;
        user.avatar = process.env.AVATAR_DEFAULT || 'uploads/user/avatar/avatar_default.png'
        user.role = userRole;
        user.isVerified = Constant.ONE;
        user.createdAt = (new Date());
        user.modifiedAt = (new Date());

        await User.save(user);

        let infor = MailService.sendEmailPassword(req, res, user, password);
        if (!infor) return false;

        return true;
    }

    static updateUser = async (req: Request, res: Response, userId: string) => {
        let user = await User.findOne(userId);
        if (!user) return false;

        user.fullName = req.body.fullName ? req.body.fullName : user.fullName;
        user.title = req.body.title ? req.body.title : user.title;
        user.gender = req.body.gender ? req.body.gender : user.gender;
        user.phoneNumber = req.body.phoneNumber ? req.body.phoneNumber : user.phoneNumber;
        user.dateOfBirth = req.body.dateOfBirth ? req.body.dateOfBirth : user.dateOfBirth;
        user.address = req.body.address ? req.body.address : user.address;
        // user.email = req.body.email ? req.body.email : user.email;
        // user.password = req.body.password ? req.body.password : user.password;
        user.modifiedAt = (new Date());
        await User.save(user);

        return true;
    }

    static uploadAvatar = async (req: Request, res: Response) => {
        //Get user ID
        let currentUserId = req.body.id;
        let user = await User.findOne({ id: currentUserId });
        let file = req['file'];
        if (!file) return res.status(400).send({ message: 'Images not found.' });

        user.avatar = file.path;
        user.modifiedAt = (new Date());
        await User.save(user);

        return res.status(200).send({ message: "Upload Avatar Successfully !" });
    }

    static getPatientUserId =  async (patientId) => {
        let patient = await getRepository(Patient)
        .createQueryBuilder('patient')
        .select('patient.userId')
        .where(`patient.id = '${patientId}'`)
        .getOne()   

        return patient.userId;
    }

    static getDoctorUserId =  async (patientId) => {
        let doctor = await getRepository(Doctor)
        .createQueryBuilder('doctor')
        .select('doctor.userId')
        .where(`doctor.id = '${patientId}'`)
        .getOne()   

        return doctor.userId;
    }

    static importExcelRow = async (row: any, acc: string, userRole: string) => {
        let user = await User.findOne({
            where: [
                { username: acc },
                { phoneNumber: row[3] },
                { email: row[6] }]
        });
        let userId = uuid();
        // Generate password
        let password = generator.generate({
            length: 8,
            numbers: true
        });
        let hashed = bcrypt.hashSync(password, saltRounds);

        user = new User();
        user.id = userId;
        user.fullName = row[0];
        user.title = row[1];
        user.gender = row[2];
        user.phoneNumber = row[3];
        user.dateOfBirth = row[4];
        user.address = row[5];
        user.email = row[6];
        user.username = acc;
        user.password = hashed;
        user.avatar = process.env.AVATAR_DEFAULT || 'uploads/user/avatar/avatar_default.png';
        user.role = userRole;
        user.isVerified = Constant.ONE;
        user.createdAt = (new Date());
        user.modifiedAt = (new Date());

        await User.save(user);

        // let infor = MailService.sendImportUserPassword(row, password);
        // if (!infor) return false;

        return userId;
    }
}

export default UserService;
