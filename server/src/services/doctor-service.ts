import { Department } from './../models/department';
import { Request, Response } from "express";
import { User } from "../models/user";
import { Staff } from "../models/staff";
import { Doctor } from "../models/doctor";
import { getRepository, getManager } from "typeorm";
import { Constant } from "../utils/constant";
import UserService from './user-service';
import { v4 as uuid } from 'uuid';
import Common from "../utils/common";
import readXlsxFile from 'read-excel-file/node';
import * as path from 'path';
import MailService from './mail-service';
const generator = require('generate-password');
import * as bcrypt from 'bcryptjs';
const saltRounds = 10;

class DoctorService {

    static createDoctor = async (req: Request, res: Response) => {
        let userId = uuid();
        let info = await UserService.createUser(req, res, userId, Constant.DOCTOR);
        if (!info) return res.status(400).send({ message: 'User already registered.' });

        let doctor = new Doctor();
        doctor.id = uuid();
        doctor.userId = userId;
        doctor.departmentId = req.body.departmentId;
        doctor.isActive = Constant.ONE;
        doctor.createdAt = (new Date());
        doctor.modifiedAt = (new Date());
        await Doctor.save(doctor);

        return res.status(200).send({ message: "Thêm bác sĩ thành công!" });
    }

    static getAllDoctor = async (req: Request, res: Response) => {
        let page = req.query.page || "1";
        let size = req.query.size || "50";
        let count, doctor, user;

        if (!Number.isInteger(Number(page)) || Number(page) < 1) page = "1";
        if (!Number.isInteger(Number(size)) || Number(size) < 1) size = "50";

        //Get user ID
        let currentUserId = await Common.getCurrentUserId(req);
        if (currentUserId !== null) {
            user = await User.findOne(currentUserId);
        }
        if (!user || user.role !== Constant.STAFF) {
            count = await getRepository(Doctor)
                .createQueryBuilder("doctor")
                .leftJoin("doctor.user", "user")
                .leftJoin("doctor.department", "department")
                .leftJoin("department.hospital", "hospital")
                .where("hospital.isActive = 1")
                .andWhere("department.isActive = 1")
                .andWhere("doctor.isActive = 1")
                .getCount();

            if (count == 0) return res.status(400).send({ message: "User Not Found." });

            if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();

            doctor = await getRepository(Doctor)
                .createQueryBuilder("doctor")
                .select(["doctor.id", "doctor.departmentId", "doctor.image", "doctor.isActive", "user.id", "user.fullName", "user.title", "user.gender",
                    "user.phoneNumber", "user.dateOfBirth", "user.address", "user.email", "user.username", "user.avatar", "department.name", "hospital.name"])
                .leftJoin("doctor.user", "user")
                .leftJoin("doctor.department", "department")
                .leftJoin("department.hospital", "hospital")
                .where("hospital.isActive = 1")
                .andWhere("department.isActive = 1")
                .andWhere("doctor.isActive = 1")
                .orderBy("doctor.id")
                .skip((Number(page) - 1) * Number(size))
                .take(Number(size))
                .getMany();
        }
        else if (user.role === Constant.STAFF) {
            let staff = await Staff.findOne({ where: { userId: user.id } });

            count = await getRepository(Doctor)
                .createQueryBuilder("doctor")
                .leftJoin("doctor.user", "user")
                .leftJoin("doctor.department", "department")
                .leftJoin("department.hospital", "hospital")
                .where(`department.hospitalId = '${staff.hospitalId}'`)
                .getCount();

            if (count == 0) return res.status(400).send({ message: "User Not Found." });

            if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();

            doctor = await getRepository(Doctor)
                .createQueryBuilder("doctor")
                .select(["doctor.id", "doctor.departmentId", "doctor.image", "doctor.isActive", "user.id", "user.fullName", "user.title", "user.gender",
                    "user.phoneNumber", "user.dateOfBirth", "user.address", "user.email", "user.username", "user.avatar", "department.name", "hospital.name"])
                .leftJoin("doctor.user", "user")
                .leftJoin("doctor.department", "department")
                .leftJoin("department.hospital", "hospital")
                .where(`department.hospitalId = '${staff.hospitalId}'`)
                .orderBy("doctor.id")
                .skip((Number(page) - 1) * Number(size))
                .take(Number(size))
                .getMany();
        }

        return res.status(200).send({
            page: {
                page_index: Number(page),
                page_size: Number(size),
                total_page: Math.ceil(count / Number(size)),
                total_item: count
            },
            data: doctor
        });
    };

    static getTopThreeDoctor = async (req: Request, res: Response) => {
        let count = await Doctor.count();
        if (count == 0) return res.status(400).send({ message: "User Not Found." });

        let doctor = await getRepository(Doctor)
            .createQueryBuilder("doctor")
            .select(["doctor.id", "doctor.departmentId", "doctor.image", "doctor.isActive", "user.id", "user.fullName", "user.title", "user.gender",
                "user.phoneNumber", "user.dateOfBirth", "user.address", "user.email", "user.username", "user.avatar", "department.name", "hospital.name"])
            .leftJoin("doctor.user", "user")
            .leftJoin("doctor.department", "department")
            .leftJoin("department.hospital", "hospital")
            .where("hospital.isActive = 1")
            .andWhere("department.isActive = 1")
            .andWhere("doctor.isActive = 1")
            .orderBy("doctor.id")
            .skip(0)
            .take(3)
            .getMany();

        return res.status(200).send(doctor);
    }
    
    static searchDoctor = async (req: Request, res: Response) => {
        let page = req.query.page || "1";
        let size = req.query.size || "10";
        let txtSearch = req.query.txtSearch;
        let departmentId = req.query.departmentId;
        let hospitalId = req.query.hospitalId;
        let count, doctor, user;

        if (!Number.isInteger(Number(page)) || Number(page) < 1) page = "1";
        if (!Number.isInteger(Number(size)) || Number(size) < 1) size = "10";

        //Get user ID
        let currentUserId = await Common.getCurrentUserId(req);
        if (currentUserId !== null) {
            user = await User.findOne(currentUserId);
        }
        // search by admin || patient || Guest
        if (!user || user.role !== Constant.STAFF) {
            //search by department
            if (departmentId) {
                // Have txtSearch
                if (txtSearch) {
                    count = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`doctor.departmentId = '${departmentId}'`)
                        .andWhere(`(user.fullName like '%${txtSearch}%' 
                                OR user.phoneNumber like '%${txtSearch}%' 
                                OR user.email like '%${txtSearch}%' 
                                OR user.username like '%${txtSearch}%')`)
                        .andWhere("hospital.isActive = 1")
                        .andWhere("department.isActive = 1")
                        .andWhere("doctor.isActive = 1")
                        .getCount();

                    if (count == 0) return res.status(400).send({ message: "User Not Found." });

                    if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();

                    doctor = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .select(["doctor.id", "doctor.departmentId", "doctor.image", "doctor.isActive", "user.id", "user.fullName", "user.title", "user.gender",
                            "user.phoneNumber", "user.dateOfBirth", "user.address", "user.email", "user.username", "user.avatar", "department.name", "hospital.name"])
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`doctor.departmentId = '${departmentId}'`)
                        .andWhere(`(user.fullName like '%${txtSearch}%' OR user.phoneNumber like '%${txtSearch}%' OR user.email like '%${txtSearch}%' OR user.username like '%${txtSearch}%')`)
                        .andWhere("hospital.isActive = 1")
                        .andWhere("department.isActive = 1")
                        .andWhere("doctor.isActive = 1")
                        .orderBy("doctor.id")
                        .skip((Number(page) - 1) * Number(size))
                        .take(Number(size))
                        .getMany();

                } else {// Done Have txtSearch
                    count = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`doctor.departmentId = '${departmentId}'`)
                        .andWhere("hospital.isActive = 1")
                        .andWhere("department.isActive = 1")
                        .andWhere("doctor.isActive = 1")
                        .getCount();

                    if (count == 0) return res.status(400).send({ message: "User Not Found." });

                    if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();

                    doctor = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .select(["doctor.id", "doctor.departmentId", "doctor.image", "doctor.isActive", "user.id", "user.fullName", "user.title", "user.gender",
                            "user.phoneNumber", "user.dateOfBirth", "user.address", "user.email", "user.username", "user.avatar", "department.name", "hospital.name"])
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`doctor.departmentId = '${departmentId}'`)
                        .andWhere("hospital.isActive = 1")
                        .andWhere("department.isActive = 1")
                        .andWhere("doctor.isActive = 1")
                        .orderBy("doctor.id")
                        .skip((Number(page) - 1) * Number(size))
                        .take(Number(size))
                        .getMany();
                }

            }
            //search by hospital
            else if (hospitalId) {
                //Have txtSearch
                if (txtSearch) {
                    count = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`department.hospitalId = '${hospitalId}'`)
                        .andWhere(`(user.fullName like '%${txtSearch}%' 
                                OR user.phoneNumber like '%${txtSearch}%' 
                                OR user.email like '%${txtSearch}%' 
                                OR user.username like '%${txtSearch}%')`)
                        .andWhere("hospital.isActive = 1")
                        .andWhere("department.isActive = 1")
                        .andWhere("doctor.isActive = 1")
                        .getCount();

                    if (count == 0) return res.status(400).send({ message: "User Not Found." });

                    if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();

                    doctor = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .select(["doctor.id", "doctor.departmentId", "doctor.image", "doctor.isActive", "user.id", "user.fullName", "user.title", "user.gender",
                            "user.phoneNumber", "user.dateOfBirth", "user.address", "user.email", "user.username", "user.avatar", "department.name", "hospital.name"])
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`department.hospitalId = '${hospitalId}'`)
                        .andWhere(`(user.fullName like '%${txtSearch}%' OR user.phoneNumber like '%${txtSearch}%' OR user.email like '%${txtSearch}%' OR user.username like '%${txtSearch}%')`)
                        .andWhere("hospital.isActive = 1")
                        .andWhere("department.isActive = 1")
                        .andWhere("doctor.isActive = 1")
                        .orderBy("doctor.id")
                        .skip((Number(page) - 1) * Number(size))
                        .take(Number(size))
                        .getMany();

                } else {// Done have txtSearch
                    count = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`department.hospitalId = '${hospitalId}'`)
                        .andWhere("hospital.isActive = 1")
                        .andWhere("department.isActive = 1")
                        .andWhere("doctor.isActive = 1")
                        .getCount();

                    if (count == 0) return res.status(400).send({ message: "User Not Found." });

                    if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();

                    doctor = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .select(["doctor.id", "doctor.departmentId", "doctor.image", "doctor.isActive", "user.id", "user.fullName", "user.title", "user.gender",
                            "user.phoneNumber", "user.dateOfBirth", "user.address", "user.email", "user.username", "user.avatar", "department.name", "hospital.name"])
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`department.hospitalId = '${hospitalId}'`)
                        .andWhere("hospital.isActive = 1")
                        .andWhere("department.isActive = 1")
                        .andWhere("doctor.isActive = 1")
                        .orderBy("doctor.id")
                        .skip((Number(page) - 1) * Number(size))
                        .take(Number(size))
                        .getMany();
                }

            }
            //search all doctor in DB
            else {
                if (txtSearch) {
                    count = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`(user.fullName like '%${txtSearch}%' 
                                OR user.phoneNumber like '%${txtSearch}%' 
                                OR user.email like '%${txtSearch}%' 
                                OR user.username like '%${txtSearch}%')`)
                        .andWhere("hospital.isActive = 1")
                        .andWhere("department.isActive = 1")
                        .andWhere("doctor.isActive = 1")
                        .getCount();

                    if (count == 0) return res.status(400).send({ message: "User Not Found." });

                    if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();

                    doctor = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .select(["doctor.id", "doctor.departmentId", "doctor.image", "doctor.isActive", "user.id", "user.fullName", "user.title", "user.gender",
                            "user.phoneNumber", "user.dateOfBirth", "user.address", "user.email", "user.username", "user.avatar", "department.name", "hospital.name"])
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`(user.fullName like '%${txtSearch}%' OR user.phoneNumber like '%${txtSearch}%' OR user.email like '%${txtSearch}%' OR user.username like '%${txtSearch}%')`)
                        .andWhere("hospital.isActive = 1")
                        .andWhere("department.isActive = 1")
                        .andWhere("doctor.isActive = 1")
                        .orderBy("doctor.id")
                        .skip((Number(page) - 1) * Number(size))
                        .take(Number(size))
                        .getMany();
                }
                else {
                    count = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where("hospital.isActive = 1")
                        .andWhere("department.isActive = 1")
                        .andWhere("doctor.isActive = 1")
                        .getCount();

                    if (count == 0) return res.status(400).send({ message: "User Not Found." });

                    if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();

                    doctor = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .select(["doctor.id", "doctor.departmentId", "doctor.image", "doctor.isActive", "user.id", "user.fullName", "user.title", "user.gender",
                            "user.phoneNumber", "user.dateOfBirth", "user.address", "user.email", "user.username", "user.avatar", "department.name", "hospital.name"])
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where("hospital.isActive = 1")
                        .andWhere("department.isActive = 1")
                        .andWhere("doctor.isActive = 1")
                        .orderBy("doctor.id")
                        .skip((Number(page) - 1) * Number(size))
                        .take(Number(size))
                        .getMany();
                }
            }
        }
        // Search by staff
        else if (user.role === Constant.STAFF) {
            let staff = await Staff.findOne({ where: { userId: user.id } });
            //search by department
            if (departmentId) {
                // Have txtSearch
                if (txtSearch) {
                    count = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`department.hospitalId = '${staff.hospitalId}'`)
                        .andWhere(`doctor.departmentId = '${departmentId}'`)
                        .andWhere(`(user.fullName like '%${txtSearch}%' 
                                OR user.phoneNumber like '%${txtSearch}%' 
                                OR user.email like '%${txtSearch}%' 
                                OR user.username like '%${txtSearch}%')`)
                        .getCount();

                    if (count == 0) return res.status(400).send({ message: "User Not Found." });

                    if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();

                    doctor = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .select(["doctor.id", "doctor.departmentId", "doctor.image", "doctor.isActive", "user.id", "user.fullName", "user.title", "user.gender",
                            "user.phoneNumber", "user.dateOfBirth", "user.address", "user.email", "user.username", "user.avatar", "department.name", "hospital.name"])
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`department.hospitalId = '${staff.hospitalId}'`)
                        .andWhere(`doctor.departmentId = '${departmentId}'`)
                        .andWhere(`(user.fullName like '%${txtSearch}%' 
                                OR user.phoneNumber like '%${txtSearch}%' 
                                OR user.email like '%${txtSearch}%' 
                                OR user.username like '%${txtSearch}%')`)
                        .orderBy("doctor.id")
                        .skip((Number(page) - 1) * Number(size))
                        .take(Number(size))
                        .getMany();

                }
                else {// Done Have txtSearch
                    count = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`department.hospitalId = '${staff.hospitalId}'`)
                        .andWhere(`doctor.departmentId = '${departmentId}'`)
                        .getCount();

                    if (count == 0) return res.status(400).send({ message: "User Not Found." });

                    if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();

                    doctor = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .select(["doctor.id", "doctor.departmentId", "doctor.image", "doctor.isActive", "user.id", "user.fullName", "user.title", "user.gender",
                            "user.phoneNumber", "user.dateOfBirth", "user.address", "user.email", "user.username", "user.avatar", "department.name", "hospital.name"])
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`department.hospitalId = '${staff.hospitalId}'`)
                        .andWhere(`doctor.departmentId = '${departmentId}'`)
                        .orderBy("doctor.id")
                        .skip((Number(page) - 1) * Number(size))
                        .take(Number(size))
                        .getMany();
                }

            }
            //search by hospital of staff
            else {
                if (txtSearch) {
                    count = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`department.hospitalId = '${staff.hospitalId}'`)
                        .andWhere(`(user.fullName like '%${txtSearch}%' 
                                OR user.phoneNumber like '%${txtSearch}%' 
                                OR user.email like '%${txtSearch}%' 
                                OR user.username like '%${txtSearch}%')`)
                        .getCount();

                    if (count == 0) return res.status(400).send({ message: "User Not Found." });

                    if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();

                    doctor = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .select(["doctor.id", "doctor.departmentId", "doctor.image", "doctor.isActive", "user.id", "user.fullName", "user.title", "user.gender",
                            "user.phoneNumber", "user.dateOfBirth", "user.address", "user.email", "user.username", "user.avatar", "department.name", "hospital.name"])
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`department.hospitalId = '${staff.hospitalId}'`)
                        .andWhere(`(user.fullName like '%${txtSearch}%' OR user.phoneNumber like '%${txtSearch}%' OR user.email like '%${txtSearch}%' OR user.username like '%${txtSearch}%')`)
                        .orderBy("doctor.id")
                        .skip((Number(page) - 1) * Number(size))
                        .take(Number(size))
                        .getMany();

                }
                else {
                    count = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`department.hospitalId = '${staff.hospitalId}'`)
                        .getCount();

                    if (count == 0) return res.status(400).send({ message: "User Not Found." });

                    if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();

                    doctor = await getRepository(Doctor)
                        .createQueryBuilder("doctor")
                        .select(["doctor.id", "doctor.departmentId", "doctor.image", "doctor.isActive", "user.id", "user.fullName", "user.title", "user.gender",
                            "user.phoneNumber", "user.dateOfBirth", "user.address", "user.email", "user.username", "user.avatar", "department.name", "hospital.name"])
                        .leftJoin("doctor.user", "user")
                        .leftJoin("doctor.department", "department")
                        .leftJoin("department.hospital", "hospital")
                        .where(`department.hospitalId = '${staff.hospitalId}'`)
                        .orderBy("doctor.id")
                        .skip((Number(page) - 1) * Number(size))
                        .take(Number(size))
                        .getMany();
                }
            }
        }

        return res.status(200).send({
            page: {
                page_index: Number(page),
                page_size: Number(size),
                total_page: Math.ceil(count / Number(size)),
                total_item: count
            },
            data: doctor
        });

    }

    static getDoctorbyId = async (req: Request, res: Response) => {
        const doctorId = req.params.id;

        const doctor = await getRepository(Doctor)
            .createQueryBuilder("doctor")
            .select(["doctor.id", "doctor.departmentId", "doctor.image", "doctor.isActive", "user.id", "user.fullName", "user.title", "user.gender",
                "user.phoneNumber", "user.dateOfBirth", "user.address", "user.email", "user.username", "user.avatar", "department.name", "hospital.name"])
            .leftJoin("doctor.user", "user")
            .leftJoin("doctor.department", "department")
            .leftJoin("department.hospital", "hospital")
            .where("doctor.id = :id", { id: doctorId })
            .andWhere("hospital.isActive = 1")
            .andWhere("department.isActive = 1")
            .andWhere("doctor.isActive = 1")
            .getOne();
        if (!doctor) return res.status(400).send({ message: "User Not Found." });

        return res.status(200).send(doctor);
    };

    static getDoctorbyUserId = async (req: Request, res: Response) => {
        const userId = req.params.id;

        const doctor = await getRepository(Doctor)
            .createQueryBuilder("doctor")
            .select(["doctor.id", "doctor.departmentId", "doctor.image", "doctor.isActive", "user.id", "user.fullName", "user.title", "user.gender",
                "user.phoneNumber", "user.dateOfBirth", "user.address", "user.email", "user.username", "user.avatar", "department.name", "hospital.name"])
            .leftJoin("doctor.user", "user")
            .leftJoin("doctor.department", "department")
            .leftJoin("department.hospital", "hospital")
            .where("user.id = :id", { id: userId })
            .andWhere("hospital.isActive = 1")
            .andWhere("department.isActive = 1")
            .andWhere("doctor.isActive = 1")
            .getOne();
        if (!doctor) return res.status(400).send({ message: "User Not Found." });

        return res.status(200).send(doctor);
    };

    static setActiveDoctor = async (req: Request, res: Response) => {
        let doctor = await Doctor.findOne(req.params.id);

        if (!doctor) return res.status(400).send({ message: "User Not Found." });

        doctor.isActive === Constant.ONE ? doctor.isActive = Constant.ZERO : doctor.isActive = Constant.ONE;
        doctor.modifiedAt = (new Date());
        await Doctor.save(doctor);

        return res.status(200).send({ message: "Set Active User Successfully !" });
    };

    static updateDoctor = async (req: Request, res: Response) => {
        let doctor = await Doctor.findOne(req.body.id);

        if (!doctor) return res.status(400).send({ message: 'User not existed.' });

        doctor.departmentId = req.body.departmentId ? req.body.departmentId : doctor.departmentId;
        doctor.isActive = req.body.isActive ? req.body.isActive : doctor.isActive;
        doctor.modifiedAt = (new Date());
        await Doctor.save(doctor);

        let infor = UserService.updateUser(req, res, doctor.userId);
        if (!infor) return res.status(400).send({ message: 'User not existed.' });

        return res.status(200).send({ message: "Cập nhật bác sĩ thành công" });
    }


    static deleteDoctor = async (req: Request, res: Response) => {
        const doctor = await Doctor.findOne(req.params.id);
        if (!doctor) return res.status(400).send({ message: "User Not Found." });

        await Doctor.delete(doctor.id);
        await User.delete(doctor.userId);

        return res.status(200).send({ message: "Delete User Successfully !" });
    };

    static importFromExcel = async (req: Request, res: Response) => {
        try {
            console.log('aaaaaaaa');
            
            if (req.file === undefined) {
              return res.status(400).send("Please upload an excel file!");
            }
            let filePath, hosId;
            let id = Common.getCurrentUserId(req);
            if (req.body.role === 'STAFF') {
                filePath = path.join(__dirname, '../../../uploads/staff/' + id + '/' + req.file.filename);
                let staff = await Staff.findOne({ where: { userId: id} });
                hosId = staff.hospitalId;
            }
            else {
                filePath = path.join(__dirname, '../../../uploads/admin/' + id + '/' + req.file.filename);
                hosId = req.body.hospitalId;
            }
            let departments = await getRepository(Department)
            .createQueryBuilder('department')
            .select([
                'department.id',
                'department.name'
            ])
            .where(`department.hospitalId = ${hosId}`)
            .getMany();
            let rawDepts = departments.map(dept => ({ id: dept.id, name: Common.minifyString(dept.name) }));

            let rowCount = 2, flag,
                users = [], user, userId, password,
                doctors = [], doctor,
                arr = [],
                tail, acc,
                tempUsers, lastIndex;
            await readXlsxFile(filePath).then(async (rows) => {
                rows.shift(); //skip header

                let row;
                for (let i = 0; i < rows.length; i++) {
                    row = rows[i];
                    flag = rawDepts.find(dept => dept.name === Common.minifyString(row[7]));
                    if (flag) {
                        rowCount ++;
                        arr = Common.minifyString(row[0]).split(' ');
                        tail = '';
                        for (let i = 0; i < arr.length - 1; i++) {tail = tail + arr[i].slice(0, 1);
                        }
                        acc = arr[arr.length -1] + tail
                        tempUsers = await getRepository(User)
                        .createQueryBuilder('user')
                        .select('user.username')
                        .where(`user.username LIKE '%${acc}%'`)
                        .andWhere(`user.role = 'DOCTOR'`)
                        .orderBy('user.username')
                        .getMany();
                        if (tempUsers.length) {
                            lastIndex = tempUsers.map(x => x.username)[tempUsers.length - 1];                    
                            lastIndex = parseInt(lastIndex.slice(lastIndex.length - 1, lastIndex.length), 10);
                            acc += (lastIndex + 1).toString();
                        }
                        else {
                            acc += 1;
                        }

                        userId = uuid();
                        password = generator.generate({
                            length: 8,
                            numbers: true
                        });
                        let hashed = bcrypt.hashSync(password, saltRounds);

                        user = new User();
                        user.id = userId;
                        user.fullName = row[0];
                        user.title = row[1];
                        user.gender = row[2] === 'Nam' ? 1 : 2;
                        user.phoneNumber = row[3];
                        user.dateOfBirth = row[4];
                        user.address = row[5];
                        user.email = row[6];
                        user.username = acc;
                        user.password = hashed;
                        user.avatar = process.env.AVATAR_DEFAULT || 'uploads/user/avatar/avatar_default.png';
                        user.role = Constant.DOCTOR;
                        user.isVerified = Constant.ONE;
                        user.createdAt = (new Date());
                        user.modifiedAt = (new Date());
                        user.rawPass = password;
                        console.log(user);
                        
                        users.push(user);
                        
                        doctor = new Doctor();
                        doctor.id = uuid();
                        doctor.userId = userId;
                        doctor.departmentId = rawDepts.find(dept => dept.name === Common.minifyString(row[7])).id;                        
                        doctor.isActive = Constant.ONE;
                        doctor.createdAt = (new Date());
                        doctor.modifiedAt = (new Date());
                        doctors.push(doctor);
                        // console.log('f*ck asynchronous (array.foreach is not promise-aware)');
                    }
                }   
            });
            if (!flag) {
                return res.status(400).send({ message: 'Không đọc được dữ liệu khoa tại dòng ' + rowCount });
            }
            else {
                Promise.all([
                    User.save(users),
                    Doctor.save(doctors)
                ]).then(resolve => {
                    console.log('done?');
                    
                    let infos = MailService.sendImportedUserPassword(users);
                    return infos ? res.status(200).send({ message: "Import success!" })
                                : res.status(400).send({ message: "Some mail error!" });
                })
                .catch(error => {
                    console.log('Import error: ', error.message);
                    res.status(400).send({ message: error.message });
                })
            }            
        } catch (error) {
            return res.status(400).send({ message: error.message });
        }
    }

    static downloadTemplate = (req: Request, res: Response) => {
        let file = path.join(__dirname, '../../../uploads/staff/blouse-doctor.xlsx');
        res.download(file); // Set disposition and send it.
    }

    static uploadImage = async (req: Request, res: Response) => {
        try {
            let userId = Common.getCurrentUserId(req);
            let doctor = await Doctor.findOne({ where: { userId: userId } });
            let file = req.file;
            if (!file) return res.status(400).send({ message: 'Images not found.' });

            doctor.image = file.path;
            doctor.modifiedAt = new Date();

            await Doctor.save(doctor);
            return res.status(200).send({ message: "Upload Image Successfully!" });
        } catch (error) {
            return res.status(400).send({ message: 'Error at upload doctor image.' });
        }
    }
};

export default DoctorService;
