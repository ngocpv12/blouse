import { Request, Response } from "express";
import { Contact } from "../models/contact";
import { Constant } from "../utils/constant";
import { getRepository, getManager } from "typeorm";
import Common from "../utils/common";
import { Admin } from "../models/admin";
import MailService from "./mail-service";

class ContactService {
    static getAllContact = async (req: Request, res: Response) => {
        let page = req.query.page || "1";
        let size = req.query.size || "10";
        let count = await Contact.count();

        if (count == 0) return res.status(400).send({ message: "Contact Not Found." });

        if (!Number.isInteger(Number(page)) || Number(page) < 1) page = "1";
        if (!Number.isInteger(Number(size)) || Number(size) < 1) size = "10";
        if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size)) || 1).toString();

        const contact = await getRepository(Contact)
            .createQueryBuilder("contact")
            .select([
                "contact.id",
                "contact.name",
                "contact.email",
                "contact.phoneNumber",
                "contact.title",
                "contact.message",
                "contact.respondentId",
                "contact.reply",
                "contact.isActive",
                "admin.id",
                "user.id",
                "user.fullName",
                "user.title",
                "user.phoneNumber",
                "user.email",
                "user.username"
            ])
            .leftJoin("contact.admin", "admin")
            .leftJoin("admin.user", "user")
            .skip((Number(page) - 1) * Number(size))
            .take(Number(size))
            .getMany();

        return res.status(200).send({
            page: {
                page_index: Number(page),
                page_size: Number(size),
                total_page: Math.ceil(count / Number(size))
            },
            data: contact
        });
    }

    static createContact = async (req: Request, res: Response) => {
        let contact = new Contact();

        contact.name = req.body.name;
        contact.email = req.body.email;
        contact.phoneNumber = req.body.phoneNumber;
        contact.title = req.body.title;
        contact.message = req.body.message;
        contact.isActive = Constant.ONE;
        contact.createdAt = (new Date());
        contact.modifiedAt = (new Date());

        await Contact.save(contact);
        return res.status(200).send({ message: "Create Contact Successfully!" });
    }

    static setActiveContact = async (req: Request, res: Response) => {
        let contact = await Contact.findOne(req.params.id);

        if (!contact) return res.status(400).send({ message: "Contact Not Found." });

        let currentUserId = await Common.getCurrentUserId(req);
        let admin = await Admin.findOne({userId: currentUserId});

        contact.isActive === Constant.ONE ? contact.isActive = Constant.ZERO : contact.isActive = Constant.ONE;
        contact.respondentId = admin.id;
        contact.modifiedAt = (new Date());

        await Contact.save(contact);

        return res.status(200).send({ message: "Set Active Contact Successfully !" });
    };

    static deleteContact = async (req: Request, res: Response) => {
        let contact = await Contact.findOne(req.params.id);
        if (!contact) return res.status(400).send({ message: "Contact Not Found." });

        await Contact.delete(contact.id);

        return res.status(200).send({ message: "Delete Contact Successfully !" });
    }

    static sendReply = async (req: Request, res: Response) => {
        try {
            let contact = await Contact.findOne(req.body.id);
            let admin = await Admin.findOne({userId: req.body.respondentId});
            contact.respondentId = admin.id;
            contact.reply = req.body.reply;
            contact.modifiedAt = (new Date());

            let mailer = MailService.sendReply(req, res);
            if (mailer) await Contact.save(contact);

            return res.status(200).send({ message: "Gửi mail trả lời thành công!" });
        } catch (error) {
            return res.status(400).send({ message: "Gửi mail trả lời thất bại!" });
        }
    }

}
export default ContactService;