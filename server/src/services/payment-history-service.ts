import { User } from './../models/user';
import { MedicalExamination } from './../models/medical-examination';
import { getRepository } from 'typeorm';
import { Request, Response } from "express";
import { PaymentHistory } from "../models/payment-history";
import { Constant } from "../utils/constant";
import config from '../config';
import crypto from 'crypto';
import https from 'https';
import MailService from './mail-service';
import Common from '../utils/common';
import { Staff } from '../models/staff';
import { v4 as uuid } from 'uuid';

class PaymentHistoryService{
    static registerPayment = async (req: Request, res: Response, medicalExaminationId: number, patientId: string) => {
        let payment = new PaymentHistory();

        //status = 0: chờ thanh toán, chưa tạo payUrl
        //status = 1: chờ thanh toán, đã tạo payUrl
        //status = 2: đã thanh toán
        //status > 9998: lỗi (mã lỗi do Momo cung cấp = status - 10000, mã 36 là payUrl hết hạn)
        //tra mã lỗi tại https://developers.momo.vn/#/docs/aio/?id=b%e1%ba%a3ng-m%c3%a3-l%e1%bb%97i

        payment.patientId = patientId;
        payment.medicalExaminationId = medicalExaminationId;
        payment.amount = Constant.INIT_AMOUNT.toString();
        payment.status = Constant.ZERO;
        payment.isActive = Constant.ONE;
        payment.createdAt = (new Date());
        payment.modifiedAt = (new Date());
        payment.purchasedAt = (new Date('0001-01-01'));

        await PaymentHistory.save(payment);
        return true;
    }

    static getAllPayment = async (req: Request, res: Response) => {
        let page = req.query.page || "1";
        let size = req.query.size || "10";
        let count = await PaymentHistory.count();

        if (!Number.isInteger(Number(page)) || Number(page) < 1) page = "1";
        if (!Number.isInteger(Number(size)) || Number(size) < 1) size = "10";
        if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size)) || 1).toString();

        const payments = await getRepository(PaymentHistory)
            .createQueryBuilder("payment")
            .select([
                "payment.id",
                "patient.id",
                "user.fullName",
                "me.id",
                "payment.amount",
                "payment.status",
                "payment.isActive",
                "payment.createdAt",
                "payment.modifiedAt"
            ])
            .leftJoin("payment.patient", "patient")
            .leftJoin("payment.medicalExamination", "me")
            .leftJoin("patient.user", "user")
            .orderBy("payment.id")
            .skip((Number(page) - 1) * Number(size))
            .take(Number(size))
            .getMany();

        if (!payments) res.status(400).send({ message: "No payments Found." });

        return res.status(200).send({
            page: {
                page_index: Number(page),
                page_size: Number(size),
                total_page: Math.ceil(count / Number(size))
            },
            data: payments
        });
    }

    static searchByPatientName = async (req: Request, res: Response) => {
        let page = req.query.page || "1";
        let size = req.query.size || "10";
        let patientId = req.query.p;
        let txtSearch = req.query.q; //p = patientId, q = searchQuery
        let count, payments: any[];

        if (!Number.isInteger(Number(page)) || Number(page) < 1) page = "1";
        if (!Number.isInteger(Number(size)) || Number(size) < 1) size = "10";
        
        let userId = Common.getCurrentUserId(req);
        let staff = await Staff.findOne({ where: { userId: userId} });
        if (staff) {
            let hosId = staff.hospitalId;

            if (txtSearch && !patientId) {
                count = await getRepository(PaymentHistory)
                    .createQueryBuilder("payment")
                    .leftJoin("payment.patient", "patient")
                    .leftJoin("patient.user", "user")
                    .leftJoin("payment.medicalExamination", "examination")
                    .leftJoin("examination.doctor", "doctor")
                    .leftJoin("doctor.department", "department")
                    .leftJoin("department.hospital", "hospital")
                    .where(`
                        user.fullName LIKE '%${txtSearch}%'
                        OR user.phoneNumber LIKE '%${txtSearch}%'
                        OR user.email LIKE '%${txtSearch}%'
                        OR user.username LIKE '%${txtSearch}%'
                    `)
                    .andWhere(`
                        hospital.id = ${hosId}
                    `)
                    .getCount();
    
                if (count == 0) return res.status(400).send({ message: "User has no payment." });
                if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();
    
                payments = await getRepository(PaymentHistory)
                    .createQueryBuilder("payment")
                    .select([
                        "payment.id",
                        "patient.id",
                        "user.id",
                        "user.fullName",
                        "examination.id",
                        "payment.amount",
                        "payment.status",
                        "payment.isActive",
                        "payment.createdAt",
                        "payment.modifiedAt"
                    ])
                    .leftJoin("payment.patient", "patient")
                    .leftJoin("patient.user", "user")
                    .leftJoin("payment.medicalExamination", "examination")
                    .leftJoin("examination.doctor", "doctor")
                    .leftJoin("doctor.department", "department")
                    .leftJoin("department.hospital", "hospital")
                    .where(`
                        user.fullName LIKE '%${txtSearch}%'
                    `)
                    .orWhere(`
                        user.phoneNumber LIKE '%${txtSearch}%'
                    `)
                    .orWhere(`
                        user.email LIKE '%${txtSearch}%'
                    `)
                    .orWhere(`
                        user.fullName LIKE '%${txtSearch}%'
                    `)
                    .andWhere(`
                        hospital.id = ${hosId}
                    `)
                    .orderBy("payment.id")
                    .skip((Number(page) - 1) * Number(size))
                    .take(Number(size))
                    .getMany();
            }
            else if (!txtSearch && patientId) {            
                count = await getRepository(PaymentHistory)
                    .createQueryBuilder("payment")
                    .leftJoin("payment.patient", "patient")
                    .leftJoin("payment.medicalExamination", "examination")
                    .leftJoin("examination.doctor", "doctor")
                    .leftJoin("doctor.department", "department")
                    .leftJoin("department.hospital", "hospital")
                    .where(`
                        patient.id = '${patientId}'
                    `)
                    .andWhere(`
                        hospital.id = ${hosId}
                    `)
                    .getCount();
    
                if (count == 0) return res.status(400).send({ message: "User has no payment." });
                if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();
    
                payments = await getRepository(PaymentHistory)
                    .createQueryBuilder("payment")
                    .select([
                        "payment.id",
                        "patient.id",
                        "user.id",
                        "user.fullName",
                        "examination.id",
                        "payment.amount",
                        "payment.status",
                        "payment.isActive",
                        "payment.createdAt",
                        "payment.modifiedAt"
                    ])
                    .leftJoin("payment.patient", "patient")
                    .leftJoin("patient.user", "user")
                    .leftJoin("payment.medicalExamination", "examination")
                    .leftJoin("examination.doctor", "doctor")
                    .leftJoin("doctor.department", "department")
                    .leftJoin("department.hospital", "hospital")
                    .where(`
                        patient.id = '${patientId}'
                    `)
                    .andWhere(`
                        hospital.id = ${hosId}
                    `)
                    .orderBy("payment.id")
                    .skip((Number(page) - 1) * Number(size))
                    .take(Number(size))
                    .getMany();
            } else if (!txtSearch && !patientId) {
                // count = await PaymentHistory.count();
                count = await getRepository(PaymentHistory)
                .createQueryBuilder("payment")
                .leftJoin("payment.medicalExamination", "examination")
                .leftJoin("examination.doctor", "doctor")
                .leftJoin("doctor.department", "department")
                .leftJoin("department.hospital", "hospital")                
                .where(`
                    hospital.id = ${hosId}
                `)
                .getCount();
                
                if (count == 0) return res.status(400).send({ message: "No payment found." });
                if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();
    
                payments = await getRepository(PaymentHistory)
                .createQueryBuilder("payment")
                .select([
                    "payment.id",
                    "patient.id",
                    "user.id",
                    "user.fullName",
                    "examination.id",
                    "payment.amount",
                    "payment.status",
                    "payment.isActive",
                    "payment.createdAt",
                    "payment.modifiedAt"
                ])
                .leftJoin("payment.patient", "patient")
                .leftJoin("patient.user", "user")
                .leftJoin("payment.medicalExamination", "examination")
                .leftJoin("examination.doctor", "doctor")
                .leftJoin("doctor.department", "department")
                .leftJoin("department.hospital", "hospital")
                .where(`
                    hospital.id = ${hosId}
                `)
                .orderBy("payment.id")
                .skip((Number(page) - 1) * Number(size))
                .take(Number(size))
                .getMany();
            } else {
                count = 0;
                payments = null;
            }
        }
        else {
            if (txtSearch && !patientId) {
                count = await getRepository(PaymentHistory)
                    .createQueryBuilder("payment")
                    .leftJoin("payment.patient", "patient")
                    .leftJoin("patient.user", "user")
                    .where(`
                        user.fullName LIKE '%${txtSearch}%'
                        OR user.phoneNumber LIKE '%${txtSearch}%'
                        OR user.email LIKE '%${txtSearch}%'
                        OR user.username LIKE '%${txtSearch}%'
                    `)
                    .getCount();
    
                if (count == 0) return res.status(400).send({ message: "User has no payment." });
                if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();
    
                payments = await getRepository(PaymentHistory)
                    .createQueryBuilder("payment")
                    .select([
                        "payment.id",
                        "patient.id",
                        "user.id",
                        "user.fullName",
                        "me.id",
                        "payment.amount",
                        "payment.status",
                        "payment.isActive",
                        "payment.createdAt",
                        "payment.modifiedAt"
                    ])
                    .leftJoin("payment.patient", "patient")
                    .leftJoin("payment.medicalExamination", "me")
                    .leftJoin("patient.user", "user")
                    .where(`
                        user.fullName LIKE '%${txtSearch}%'
                    `)
                    .orWhere(`
                        user.phoneNumber LIKE '%${txtSearch}%'
                    `)
                    .orWhere(`
                        user.email LIKE '%${txtSearch}%'
                    `)
                    .orWhere(`
                        user.fullName LIKE '%${txtSearch}%'
                    `)
                    .orderBy("payment.id")
                    .skip((Number(page) - 1) * Number(size))
                    .take(Number(size))
                    .getMany();
            }
            else if (!txtSearch && patientId) {            
                count = await getRepository(PaymentHistory)
                    .createQueryBuilder("payment")
                    .leftJoin("payment.patient", "patient")
                    .where(`
                        patient.id = '${patientId}'
                    `)
                    .getCount();
    
                if (count == 0) return res.status(400).send({ message: "User has no payment." });
                if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();
    
                payments = await getRepository(PaymentHistory)
                    .createQueryBuilder("payment")
                    .select([
                        "payment.id",
                        "patient.id",
                        "user.id",
                        "user.fullName",
                        "me.id",
                        "payment.amount",
                        "payment.status",
                        "payment.isActive",
                        "payment.createdAt",
                        "payment.modifiedAt"
                    ])
                    .leftJoin("payment.patient", "patient")
                    .leftJoin("payment.medicalExamination", "me")
                    .leftJoin("patient.user", "user")
                    .where(`
                        patient.id = '${patientId}'
                    `)
                    .orderBy("payment.id")
                    .skip((Number(page) - 1) * Number(size))
                    .take(Number(size))
                    .getMany();
            } else if (!txtSearch && !patientId) {
                count = await PaymentHistory.count();
                
                if (count == 0) return res.status(400).send({ message: "No payment found." });
                if (Number(page) > Math.ceil(count / Number(size))) page = (Math.ceil(count / Number(size))).toString();
    
                payments = await getRepository(PaymentHistory)
                .createQueryBuilder("payment")
                .select([
                    "payment.id",
                    "patient.id",
                    "user.id",
                    "user.fullName",
                    "me.id",
                    "payment.amount",
                    "payment.status",
                    "payment.isActive",
                    "payment.createdAt",
                    "payment.modifiedAt"
                ])
                .leftJoin("payment.patient", "patient")
                .leftJoin("payment.medicalExamination", "me")
                .leftJoin("patient.user", "user")
                .orderBy("payment.id")
                .skip((Number(page) - 1) * Number(size))
                .take(Number(size))
                .getMany();
            } else {
                count = 0;
                payments = null;
            }
        }

        if (!payments) return res.status(400).send({ message: "No payments Found." });

        return res.status(200).send({
            page: {
                page_index: Number(page),
                page_size: Number(size),
                total_page: Math.ceil(count / Number(size)),
                total_item: count
            },
            data: payments
        });
    }

    static getMomoPayURL = async (req: Request, res: Response) => {
        let payment = await PaymentHistory.findOne(req.body.orderId);
        let patientId = payment.patientId;
        // console.log('payment', payment);
        if (payment.status !== 0) {
            payment.isActive = Constant.ZERO;
            await PaymentHistory.save(payment);
            
            payment = new PaymentHistory();
    
            payment.patientId = patientId;
            payment.medicalExaminationId = req.body.requestId;
            payment.amount = Constant.INIT_AMOUNT.toString();
            payment.status = Constant.ZERO;
            payment.isActive = Constant.ONE;
            payment.createdAt = (new Date());
            payment.modifiedAt = (new Date());
            payment.purchasedAt = (new Date('0001-01-01'));
    
            await PaymentHistory.save(payment);
        }
        // console.log('payment 0.1', payment);

        const endpoint = config.API_ENDPOINT;
        const partnerCode = config.PARTNER_CODE;
        const accessKey = config.ACCESS_KEY;
        const secretKey = config.SECRET_KEY;
    
        const orderInfo = req.body.orderInfo;
        const returnUrl = config.CLIENT_URL + "/manage-index/manage-schedule"
        // const notifyUrl = config.SERVER_URL + "/notify";
        const notifyUrl = "https://webhook.site/789f94fe-af10-41a7-9705-a5af3477ab1f";
        const amount = req.body.amount;
        const orderId = payment.id.toString();
        const requestId = req.body.requestId;
        const requestType = "captureMoMoWallet";
        const extraData = JSON.stringify(req.body.extraData);
    
        const rawSignature = 
            "partnerCode="+partnerCode+
            "&accessKey="+accessKey+
            "&requestId="+requestId+
            "&amount="+amount+
            "&orderId="+orderId+
            "&orderInfo="+orderInfo+
            "&returnUrl="+returnUrl+
            "&notifyUrl="+notifyUrl+
            "&extraData="+extraData;
        const signature = crypto.createHmac('sha256', secretKey)
                            .update(rawSignature)
                            .digest('hex');        

        const body = JSON.stringify({
            accessKey : accessKey,
            partnerCode : partnerCode,
            requestType : requestType,
            notifyUrl : notifyUrl,
            returnUrl : returnUrl,
            orderId : orderId,
            amount : amount,
            orderInfo : orderInfo,
            requestId : requestId,
            extraData : extraData,
            signature : signature,
        });    
        
        //Create the HTTPS objects
        const options = {
            hostname: config.MOMO_HOSTNAME,
            port: 443,
            path: config.MOMO_PATH,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(body)
            }
        };    
    
        const request = https.request(options, response => {
            response.setEncoding('utf8');
            response.on('data', async (body) => {
                if (JSON.parse(body).errorCode === 0) {
                    // payment.orderInfo = orderInfo;
                    payment.amount = amount;
                    payment.status = Constant.ONE;
                    payment.modifiedAt = (new Date());
                    
                    await PaymentHistory.save(payment);
                    res.status(200).send(JSON.parse(body));
                }
                else {
                    payment.status = 10000 + parseInt(JSON.parse(body).errorCode);
                    payment.isActive = Constant.ZERO;
                    payment.modifiedAt = (new Date());
                    
                    await PaymentHistory.save(payment);
                    res.status(400).send(JSON.parse(body));
                }
            });
            response.on('end', () => {
            //   console.log('No more data in response.');
            });
        });
    
        request.on('error', (e) => {
            console.log(`problem with request: ${e.message}`);
        });

        // write data to request body
        request.write(body);
        request.end();
    }
    
    static catchMomoNotification = async (req: Request, res: Response) => {
        try {            
            console.log('payment1', req.body);    
            let payment = await PaymentHistory.findOne(req.body.orderId);
            console.log('payment2', payment);
            console.log('payment3', req.body.errorCode == 0);
            let examination = await MedicalExamination.findOne(req.body.requestId);
            let mailer;

            if (parseInt(req.body.errorCode) == 0) { //errorCode == 0 ~ success
                payment.status = Constant.TWO;
                payment.purchasedAt = req.body.responseTime;

                examination.status = Constant.TWO;
                examination.modifiedAt = new Date();

                // mailer = await MailService.sendPaymentNotification(req, res);
            }
            else {
                payment.status = 10000 + parseInt(req.body.errorCode);
                payment.isActive = Constant.ZERO;
            }
            payment.modifiedAt = new Date();
            await PaymentHistory.save(payment);
            await MedicalExamination.save(examination);

            if (req.body.errorCode == 0) {
                let user = await User.findOne(Common.getCurrentUserId(req));
                mailer = MailService.sendPaymentNotification(req, res, user);
                return res.status(200).send({ message: req.body.localMessage });
            }
            else {
                return res.status(400).send({ message: req.body.localMessage });
            }
        } catch (error) {
            res.status(400).send({ message: "Có lỗi xảy ra!" });
        }
    }
}
export default PaymentHistoryService;