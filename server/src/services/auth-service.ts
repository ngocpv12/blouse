import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { User } from "../models/user";
import { Patient } from "../models/patient";
import { v4 as uuid } from 'uuid';
import { Constant } from "../utils/constant";
import MailService from "../services/mail-service";
const generator = require('generate-password');
import Common from "../utils/common";
import * as bcrypt from 'bcryptjs';
const saltRounds = 10;

class AuthService {
    static login = async (req: Request, res: Response) => {
        let { username, password } = req.body;
        if (!(username && password)) return res.status(400).send("Invalid Username or Password.");

        try {
            let user = await User.findOne({ username: username });

            if (!user) user = await User.findOne({ where: { email: username }});

            if (!user) return res.status(400).send("Invalid Username.");

            if (!user.isVerified) return res.status(400).send("Account Is Not Verify.");

	    let truePass = false;
	    let compare = bcrypt.compareSync(password, user.password);
	    if (compare) truePass = true;
	    if (!truePass) {
            	if (user.password == password) truePass = true;
	    }
            if (!truePass) return res.status(400).send("Invalid Password.");

            const token = jwt.sign({ id: user.id, username: user.username, role: user.role }, process.env.JWT_SECRET || '@Hung123', { expiresIn: "1h" });
            
            return res
                .status(200)
                .header("Access-Control-Expose-Headers", "access_token")
                .header("access_token", token)
                // .header("Set-Cookie", "access_token=" + token)
                .send({ message: "Login Successful." });

        } catch (error) {
            return res.status(400).send(error);
        }
    };

    static oauthGoogle = async (req: Request, res: Response) => {
        let user = await User.findOne({ email: req.body.email });
        if (!user) {
            // Creat new user
            user = new User();
            user.id = uuid();
            user.fullName = req.body.name;
            user.email = req.body.email;
            user.role = Constant.PATIENT;
            user.avatar = process.env.AVATAR_DEFAULT || 'uploads/user/avatar/avatar_default.png'
            user.isVerified = Constant.ONE;
            user.createdAt = (new Date());
            user.modifiedAt = (new Date());

            //Creat new patient
            let patient = new Patient();
            patient.id = uuid();
            patient.userId = user.id;
            patient.isActive = Constant.ONE;
            patient.createdAt = (new Date());
            patient.modifiedAt = (new Date());

            await User.save(user);
            await Patient.save(patient);
        }

        const token = jwt.sign({ id: user.id, role: user.role }, process.env.JWT_SECRET || '@Hung123', { expiresIn: "1h" });
        return res
            .status(200)
            .header("Access-Control-Expose-Headers", "access_token")
            .header("access_token", token)
            .send({ message: "Login Successful." });
    };

    static resetPassword = async (req: Request, res: Response) => {
        let email = req.body.email;
        let user = await User.findOne({ email: email });
        if (!user) return res.status(400).send({ message: "User Not Found." });
        // Generate password
        let password = generator.generate({
            length: 8,
            numbers: true
        });
        let hashed = bcrypt.hashSync(password, saltRounds);
        user.password = hashed;
        await User.save(user);

        let infor = MailService.sendEmailPassword(req, res, user, password);
        if (!infor) return res.status(400).send({ message: "Send Email Password Not False." });

        return res.status(200).send({ message: "Đặt lại mật khẩu thành công, kiểm tra email để lấy mật khẩu mới" });
    };

    static updatePassword = async (req: Request, res: Response) => {
        let currentPassword = req.body.currentPassword;
        let newPassword = req.body.newPassword;
        //Get user ID
        let currentUserId = await Common.getCurrentUserId(req);
        if (currentUserId === null) return res.status(400).send({ message: "User Not Found." });
        let user = await User.findOne({ id: currentUserId });

        let compare = bcrypt.compareSync(currentPassword, user.password);
        if (!compare) return res.status(400).send("Invalid Password.");
        let hashed = bcrypt.hashSync(newPassword, saltRounds);
        
        user.password = hashed;
        await User.save(user);

        return res.status(200).send({ message: "Update Password Successful!" });
    };

}
export default AuthService;
