import { Medicine } from './../models/medicine';
import { getRepository } from 'typeorm';
import { Request, Response } from "express";

class MedicineService{
    static getByFirstLetters = async (req: Request, res: Response) => {
        let firstLetters = req.query.q;

        let medicines = await getRepository(Medicine)
            .createQueryBuilder("medicine")
            .select([
                "medicine.id",
                "medicine.name",
                "medicine.active_material",
                "medicine.concentration"
            ])
            .distinct(true)
            .where(`medicine.name LIKE '${firstLetters}%'`)
            .andWhere("medicine.classification IS NOT NULL")
            .andWhere("medicine.isActive = 1")
            .orderBy("medicine.id")
            .getMany();

        if (medicines.length === 0) return res.status(400).send({ message: "No medicine found." });

        res.status(200).send({
            data: medicines
        })
    }
}

export default MedicineService;