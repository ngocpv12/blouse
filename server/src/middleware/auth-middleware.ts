import { User } from './../models/user';
import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";

export const authToken = async (req: Request, res: Response, next: NextFunction) => {
    let token = <string>req.headers["access_token"];
    if (!token) return res.status(400).send('Access denied. No token provided.');
    try {
        //Validate the token and get data
        const decoded = <any>jwt.verify(token, process.env.JWT_SECRET || '@Hung123');
        res.locals.jwtPayload = decoded;

        //The token is valid for 1 hour
        //Send a new token on every request
        const { id, role } = decoded;
        const newToken = jwt.sign({ id, role }, process.env.JWT_SECRET || '@Hung123', { expiresIn: "1h" });

        let user = await User.findOne(id);
        if (!user) throw new Error('Invalid token!');

        res.header("Access-Control-Expose-Headers", "access_token")
            .header("access_token", newToken)
    } catch (error) {
        res.status(400).send('Invalid token!');
        return;
    }
    next();
};

export const checkRole = (roles: Array<string>) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        //Get the Profile ID from previous midleware
        const id = res.locals.jwtPayload.id;
        try {
            const user = await User.findOneOrFail(id);
            //Check if array of authorized roles includes the user's role
            if (roles.indexOf(user.role) > -1) next();
            else res.status(400).send("Access Denied !");
        } catch (error) {
            res.status(400).send(error);
        }
    };
};