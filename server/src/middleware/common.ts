const whitelist = [
  'http://localhost:4200',
  'http://localhost:4201',
  'http://localhost:4202',
  'http://localhost:8080',
  'https://blousevn.herokuapp.com',
  'https://blouse-admin.herokuapp.com',
  'https://blouse-api.herokuapp.com',
  'http://blouse-test.s3-website-ap-southeast-1.amazonaws.com',
  'http://blouse-admin.s3-website-ap-southeast-1.amazonaws.com',
  'http://blouse-client.s3-website-ap-southeast-1.amazonaws.com',
  'http://ec2-54-169-236-27.ap-southeast-1.compute.amazonaws.com:3001/'
];
export const corsOptions = {
  origin: function (origin: any, callback: any) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  },
  credentials: true
}