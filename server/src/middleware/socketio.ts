import http from 'http';
import MedicalExaminationService from '../services/medical-examination-service';
import StaffService from '../services/staff-service';
import UserService from '../services/user-service';
const io = require('socket.io')(http);

let userOnline = [];
let doctorOnline = [];
let staffOnline = [];

io.on('connection', (socket) => {
  // console.log('connected: ', socket.id);

  socket.on('disconnect', () => {
    let uIndex = userOnline.findIndex(x => x.socketId === socket.id);
    let sIndex = staffOnline.findIndex(x => x.socketId === socket.id);
    let dIndex = doctorOnline.findIndex(x => x.socketId === socket.id);

    if (uIndex > -1) {
      userOnline.splice(uIndex, 1);
      console.log("user left ", userOnline);
    }
    if (sIndex > -1) {
      staffOnline.splice(sIndex, 1);
      console.log("staff left ", staffOnline);
    }
    if (dIndex > -1) {
      doctorOnline.splice(dIndex, 1);
      console.log("doctor left ", doctorOnline);
    }

    // console.log('disconnected ', socket.id);
  });

  socket.on('userLogin', async obj => {
    userOnline.push({
      userId: obj.userId,
      socketId: socket.id
    });
    let flag = await MedicalExaminationService.patientHasCfedExam(obj.userId);
    if (flag) {
      io.to(`${socket.id}`).emit('confirmed', 'Đơn khám của bạn đã được xác nhận');
    }
    console.log("user login here ", userOnline);
  });

  socket.on('userLogout', () => {
    let index = userOnline.findIndex(x => x.socketId === socket.id);
    userOnline.splice(index, 1);
    console.log("user logout here ", userOnline);
  });

  socket.on('doctorLogin', async obj => {
    doctorOnline.push({
      doctorId: obj.doctorId,
      socketId: socket.id
    });
    let flag = await MedicalExaminationService.doctorHasCfedExam(obj.doctorId);
    if (flag) {
      io.to(`${socket.id}`).emit('confirmed', 'Có đơn khám mới được chỉ định cho bạn');
    }
    console.log("doctor login here ", doctorOnline);
  });

  socket.on('doctorLogout', () => {
    let index = doctorOnline.findIndex(x => x.socketId === socket.id);
    doctorOnline.splice(index, 1);
    console.log("doctor logout here ", doctorOnline);
  });

  socket.on('staffLogin', async obj => {
    staffOnline.push({
      staffId: obj.staffId,
      socketId: socket.id
    });
    let flag = await MedicalExaminationService.staffHasNewExam(obj.staffId);
    if (flag) {
      io.to(`${socket.id}`).emit('newRequest', 'Có đơn khám mới cần xác nhận');
    }
    console.log("staff login here ", staffOnline);
  });

  socket.on('staffLogout', () => {
    let index = staffOnline.findIndex(x => x.socketId === socket.id);
    staffOnline.splice(index, 1);
    console.log("staff logout here ", staffOnline);
  });

  socket.on('sendRequest', async obj => {
    console.log("new rq ", obj);
    // console.log("new ?? ", userOnline);
    let staffs = await StaffService.getStaffsIdByDoctorId(obj.doctorId);
    // console.log("staffs ?? ", staffs);
    // console.log("staffs ?? ", staffOnline);

    staffOnline.forEach(element => {
      if (staffs.includes(element.staffId)) {
        // console.log("new rq sent ", element.socketId);
        io.to(`${element.socketId}`).emit('newRequest', 'Có đơn khám mới cần xác nhận');
      }
    });
  });

  socket.on('confirmed', async obj => {
    console.log("confirm ", obj);
    let userId = await UserService.getPatientUserId(obj.userId);
    let doctorId = await UserService.getDoctorUserId(obj.doctorId);
    // console.log('id here ', userId, doctorId);
    
    //thông báo tới người dùng tạo đơn nếu online
    userOnline.forEach(element => {
      if (userId === element.userId) {
        // console.log("confirm to ", element.socketId);
        io.to(`${element.socketId}`).emit('confirmed', 'Đơn khám của bạn đã được xác nhận');
      }
    });
    //thông báo tới bác sĩ nếu online
    doctorOnline.forEach(element => {
      if (doctorId === element.doctorId) {
        // console.log("confirm to ", element.socketId);
        io.to(`${element.socketId}`).emit('confirmed', 'Có đơn khám mới được chỉ định cho bạn');
      }
    });
  });
});

export default io;