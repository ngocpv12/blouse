import { Router } from "express";
import ContactService from "../services/contact-service";
import { Constant } from "../utils/constant";
import { authToken, checkRole } from "../middleware/auth-middleware";

const router = Router();

//Get all Contact
router.get("/", [authToken, checkRole([Constant.ADMIN])], ContactService.getAllContact);

//Create new Contact
router.post("/", ContactService.createContact);

//set active Contact
router.put("/setActive/:id", [authToken, checkRole([Constant.ADMIN])], ContactService.setActiveContact);

//Delete new Contact
router.delete("/:id", [authToken, checkRole([Constant.ADMIN])], ContactService.deleteContact);

//send reply email
router.put("/reply", [authToken, checkRole([Constant.ADMIN])], ContactService.sendReply);

export default router;