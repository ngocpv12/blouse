import { Router } from "express";
import FeedbackService from "../services/feedback-service";
import { Constant } from "../utils/constant";
import { authToken, checkRole } from "../middleware/auth-middleware";

const router = Router();

//Get Feedback by doctor ID
router.get("/:id", FeedbackService.getFeedbackbyDoctorId);

//Create new Feedback
router.post("/", authToken, FeedbackService.createFeedback);

//Delete new Feedback
router.delete("/:id", [authToken, checkRole([Constant.ADMIN])], FeedbackService.deleteFeedback);

export default router;