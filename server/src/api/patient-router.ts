import { Router } from "express";
import PatientService from "../services/patient-service";
import { Constant } from "../utils/constant";
import { authToken, checkRole } from "../middleware/auth-middleware";

const router = Router();

//Get all Patient
router.get("/", [authToken, checkRole([Constant.STAFF, Constant.ADMIN])], PatientService.getAllPatient);

//Search Patient
router.get("/search", [authToken, checkRole([Constant.STAFF, Constant.ADMIN])], PatientService.searchPatient);

//Get one Patient by ID
router.get("/:id", [authToken], PatientService.getPatientById);

//Get one Patient by User ID
router.get("/patient-by-userId/:id", [authToken], PatientService.getPatientByUserId);

//Register new Patient
router.post("/register", PatientService.registerPatient);

//Update Patient
router.put("/", [authToken], PatientService.updatePatient);

//Set active one Patient
router.put("/setActive/:id", [authToken, checkRole([Constant.STAFF, Constant.ADMIN])], PatientService.setActivePatient);

//Delete one Patient
// router.delete("/:id", [authToken, checkRole([Constant.ADMIN])], PatientService.deletePatient);

export default router;