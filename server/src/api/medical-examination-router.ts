import { Constant } from './../utils/constant';
import { Router } from "express";
import MedicalExaminationService from "../services/medical-examination-service";
import { authToken, checkRole } from "../middleware/auth-middleware";

const router = Router();
//register Medical Examination
router.post("/", [authToken], MedicalExaminationService.registerMedicalExamination);

//get Medical Examination by patientId
router.get("/", [authToken], MedicalExaminationService.searchByPatientName);

//staff confirm Medical Examination
router.put("/confirm", [authToken], MedicalExaminationService.confirmExamination);

//cancel Medical Examination
router.put("/cancel", [authToken], MedicalExaminationService.cancelExamination);

//doctor fill Medical Record and close Medical Examination
router.put("/close/:examId", [authToken], checkRole([Constant.DOCTOR]), MedicalExaminationService.closeExamination);

//get Medical Examination by doctorId
router.get("/by-doctor", [authToken], checkRole([Constant.DOCTOR]), MedicalExaminationService.getExaminationByDoctor);

export default router;