import { Router } from "express";
import AppointmentService from "../services/appointment-service";
import { Constant } from "../utils/constant";
import { authToken, checkRole } from "../middleware/auth-middleware";

const router = Router();

//Get all appointment
router.get("/", authToken, AppointmentService.getAllAppointment);

//Search appointment
router.get("/search", authToken, AppointmentService.searchAppointment);

//Get appointment by id
router.get("/:id", authToken, AppointmentService.getAppointmentById);

//Create appointment
router.post("/", authToken, AppointmentService.createAppointment);

//Update appointment
router.put("/", [authToken, checkRole([Constant.ADMIN, Constant.STAFF])],AppointmentService.updateAppointment);

//Set active appointment
router.put("/setActive/:id",[authToken, checkRole([Constant.ADMIN, Constant.STAFF])], AppointmentService.setActiveAppointment);

export default router;