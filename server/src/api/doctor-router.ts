import { Router } from "express";
import DoctorService from "../services/doctor-service";
import { Constant } from "../utils/constant";
import { authToken, checkRole } from "../middleware/auth-middleware";
import Common from "../utils/common";

const router = Router();

//Get all Doctor
router.get("/", DoctorService.getAllDoctor);

//Get top 3 Doctor
router.get("/top-three", DoctorService.getTopThreeDoctor);

//Search Doctor
router.get("/search", DoctorService.searchDoctor);

//download excel template
router.get("/template-file", [authToken, checkRole([Constant.ADMIN, Constant.STAFF])], DoctorService.downloadTemplate);

//Get one Doctor by ID
router.get("/:id", [authToken], DoctorService.getDoctorbyId);

//Get one Doctor by User ID
router.get("/user/:id", [authToken], DoctorService.getDoctorbyUserId);

//Create new Doctor
router.post("/", [authToken, checkRole([Constant.STAFF, Constant.ADMIN])], DoctorService.createDoctor);

//Update new Doctor
router.put("/", [authToken, checkRole([Constant.STAFF, Constant.ADMIN])], DoctorService.updateDoctor);

//Set active one Doctor
router.put("/setActive/:id", [authToken, checkRole([Constant.STAFF, Constant.ADMIN])], DoctorService.setActiveDoctor);

//Upload an Excel file and import it
router.post("/import", [authToken, checkRole([Constant.ADMIN, Constant.STAFF])], Common.uploadExcel.single('excel'), DoctorService.importFromExcel);

//Delete one Doctor
// router.delete("/:id", [authToken, checkRole([Constant.ADMIN])], DoctorService.deleteDoctor);

//Upload doctor image
router.post("/upload-image", [authToken, checkRole([Constant.DOCTOR])], Common.uploadDoctorImage.single('d-image'), DoctorService.uploadImage);


export default router;