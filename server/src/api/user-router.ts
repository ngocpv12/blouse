import { Router } from "express";
import UserService from "../services/user-service";
import Common from "../utils/common";

const router = Router();
//verify user
router.get("/verify-user/:token", UserService.verifyUser);
//upload avatar
router.post("/upload-avatar", Common.upload.single('avatar'), UserService.uploadAvatar);

export default router;