import { Router } from "express";
import MedicalRecordService from "../services/medical-record-service";
import { authToken } from "../middleware/auth-middleware";

const router = Router();

//Get all MedicalRecord
router.get("/", [authToken], MedicalRecordService.getAllMedicalRecord);

//Search MedicalRecord
router.get("/search", [authToken], MedicalRecordService.searchMedicalRecord);

//Get MedicalRecord by id
router.get("/:id", [authToken], MedicalRecordService.getMedicalRecordById);

//Create MedicalRecord
router.post("/", [authToken], MedicalRecordService.createMedicalRecord);

//Update MedicalRecord
router.put("/", [authToken], MedicalRecordService.updateMedicalRecord);

//Set active MedicalRecord
router.put("/setActive/:id", [authToken], MedicalRecordService.setActiveMedicalRecord);

//get Medical Record by Medical Examination
router.get("/by-exam/:examinationId", [authToken], MedicalRecordService.getRecordByExaminationId);

export default router;