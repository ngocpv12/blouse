import { Constant } from './../utils/constant';
import { Router } from "express";
import MedicineService from '../services/medicine-service';
import { authToken, checkRole } from "../middleware/auth-middleware";

const router = Router();

//get medicines by first letters
router.get("/search", [authToken], checkRole([Constant.DOCTOR, Constant.ADMIN]), MedicineService.getByFirstLetters);

export default router;