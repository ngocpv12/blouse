import { Constant } from './../utils/constant';
import { Router } from "express";
import PaymentHistoryService from "../services/payment-history-service";
import { authToken, checkRole } from "../middleware/auth-middleware";

const router = Router();

//Get by patient or not
router.get("/", authToken, checkRole([Constant.STAFF, Constant.ADMIN]), PaymentHistoryService.searchByPatientName);

//Get momo payUrl
router.post('/send', authToken, checkRole([Constant.PATIENT]), PaymentHistoryService.getMomoPayURL);

//Catch momo notification
router.post('/notify', PaymentHistoryService.catchMomoNotification);

export default router;