import { Router } from "express";
import AuthController from "../services/auth-service";
import { authToken } from "../middleware/auth-middleware";

const router = Router();
//Login route
router.post("/login", AuthController.login);

//oauth google
router.post("/oauth/google", AuthController.oauthGoogle);

//update password
router.post("/update-password", [authToken], AuthController.updatePassword);

//reset password
router.post("/reset-password", AuthController.resetPassword);

export default router;