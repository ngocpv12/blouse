import { Router } from "express";
import HospitalService from "../services/hospital-service";
import { Constant } from "../utils/constant";
import { authToken, checkRole } from "../middleware/auth-middleware";

const router = Router();

//Get all hospital
router.get("/", HospitalService.getAllHospital);

//Get Name hospital
router.get("/name", HospitalService.getNameHospital);

//Search hospital
router.get("/search", [authToken, checkRole([Constant.ADMIN])], HospitalService.searchHospital);

//get id by staff id
router.get("/by-staff", [authToken, checkRole([Constant.STAFF])], HospitalService.getHosIdByStaffId);

//Get one hospital by ID
router.get("/:id", [authToken, checkRole([Constant.ADMIN, Constant.STAFF])], HospitalService.getHospitalById);

//Create new hospital
router.post("/", [authToken, checkRole([Constant.ADMIN])], HospitalService.createHospital);

//Update new hospital
router.put("/", [authToken, checkRole([Constant.ADMIN])], HospitalService.updateHospital);

//Set Active hospital
router.put("/setActive/:id", [authToken, checkRole([Constant.ADMIN])], HospitalService.setActiveHospital);

export default router;