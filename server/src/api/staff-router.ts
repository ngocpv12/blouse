import { Router } from "express";
import StaffService from "../services/staff-service";
import { Constant } from "../utils/constant";
import { authToken, checkRole } from "../middleware/auth-middleware";

const router = Router();

//Get all staff
router.get("/", [authToken, checkRole([Constant.ADMIN])], StaffService.getAllStaff);

//Search staff
router.get("/search", [authToken, checkRole([Constant.ADMIN])], StaffService.searchStaff);

//Get one staff by ID
router.get("/:id", [authToken, checkRole([Constant.ADMIN])], StaffService.getStaffbyId);

//Create new staff
router.post("/", [authToken, checkRole([Constant.ADMIN])], StaffService.createStaff);

//Update new staff
router.put("/:id", [authToken, checkRole([Constant.ADMIN])], StaffService.updateStaff);

//Set active one staff
router.put("/setActive/:id", [authToken, checkRole([Constant.ADMIN])], StaffService.setActiveStaff);

//Delete one staff
// router.delete("/:id", [authToken, checkRole([Constant.ADMIN])], StaffService.deleteStaff);

export default router;