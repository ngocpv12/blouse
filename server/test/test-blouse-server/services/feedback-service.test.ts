import server from '../../../src/server-test';
import { DatabaseManager } from '../../database-test-manager';
import * as jwt from "jsonwebtoken";

const supertest = require('supertest');

describe('Test feedback-service', () => {
    let request = null;
    let access_token= null;

    beforeEach(async () => {
        await DatabaseManager.init();
        await DatabaseManager.clearData();
        await DatabaseManager.insertData();
        request = supertest(server);
        access_token = jwt.sign({ id: 1, role: 'ADMIN' }, process.env.JWT_SECRET || '@Hung123', { expiresIn: "1h" });
    });

    afterAll(async (done) => {
        await DatabaseManager.close();
        done();
    });

    it('Get feedback by doctorID', () => {
        return request
            .get('/feedback/1')
            .set('Accept', 'application/json')
            .then(response => {
                expect(response.status).toBe(200);
            });
    });

    it('Delete feedback by ID', () => {
        return request
            .delete('/feedback/1')
            .set('Accept', 'application/json')
            .set('access_token', access_token)
            .then(response => {
                expect(response.status).toBe(200);
            });
    });

    it('Create feedback', () => {
        return request
            .post('/feedback/')
            .send({ medicalExaminationId: 2, rating: '5'})
            .set('Accept', 'application/json')
            .set('access_token', access_token)
            .then(response => {
                expect(response.status).toBe(200);
            });
    });

});